# inheritance

- [Master the JavaScript Interview: What’s the Difference Between Class & Prototypal Inheritance?](https://medium.com/javascript-scene/master-the-javascript-interview-what-s-the-difference-between-class-prototypal-inheritance-e4cd0a7562e9)
- [Object.create()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create)

## inheritance in Node.js
- [inherits - Easy simple tiny inheritance in JavaScript](https://github.com/isaacs/inherits/blob/master/inherits_browser.js)

```js
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    if (superCtor) {
      ctor.super_ = superCtor
      ctor.prototype = Object.create(superCtor.prototype, {
        constructor: {
          value: ctor,
          enumerable: false,
          writable: true,
          configurable: true
        }
      })
    }
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    if (superCtor) {
      ctor.super_ = superCtor
      var TempCtor = function () {}
      TempCtor.prototype = superCtor.prototype
      ctor.prototype = new TempCtor()
      ctor.prototype.constructor = ctor
    }
  }
}
```

## inheritance
### Prototype

```js
function inherit(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype)
  subClass.prototype.constructor = subClass
}
```

### usage in webpack

```js
Compiler.prototype = Object.create(Tapable.prototype);
Compiler.prototype.constructor = Compiler;
```

### extends

## mixin

```js
function copyProperties(from, to) {
	for(var key in from)
		to[key] = from[key];
	return to;
}

Tapable.mixin = function mixinTapable(pt) {
	copyProperties(Tapable.prototype, pt);
};
```

## copy properties

```js
// from immer
export const ownKeys: (target: AnyObject) => PropertyKey[] =
	typeof Reflect !== "undefined" && Reflect.ownKeys
		? Reflect.ownKeys
		: typeof Object.getOwnPropertySymbols !== "undefined"
		? obj =>
				Object.getOwnPropertyNames(obj).concat(
					Object.getOwnPropertySymbols(obj) as any
				)
    : /* istanbul ignore next */ Object.getOwnPropertyNames

export function shallowCopy(base: any, invokeGetters = false) {
	if (Array.isArray(base)) return base.slice()
	const clone = Object.create(Object.getPrototypeOf(base))
	ownKeys(base).forEach(key => {
		if (key === DRAFT_STATE) {
			return // Never copy over draft state.
		}
		const desc = Object.getOwnPropertyDescriptor(base, key)!
		let {value} = desc
		if (desc.get) {
			if (!invokeGetters) {
				throw new Error("Immer drafts cannot have computed properties")
			}
			value = desc.get.call(base)
		}
		if (desc.enumerable) {
			clone[key] = value
		} else {
			Object.defineProperty(clone, key, {
				value,
				writable: true,
				configurable: true
			})
		}
	})
	return clone
}
```
### 实现instanceOf
- [How to check if a Javascript Class inherits another (without creating an obj)?](https://stackoverflow.com/questions/14486110/how-to-check-if-a-javascript-class-inherits-another-without-creating-an-obj/45656957#45656957)

[Object.getPrototypeOf](https://johnresig.com/blog/objectgetprototypeof/)

```js
function instanceOf(a, b) {
	let proto = getPropertyOf(a)
	while (proto) {
		if (proto === b.prototype) return true
		proto = getPropertyOf(proto)
	}

	return false
}

const getPropertyOf = obj => {
	if (typeof Object.getPrototypeOf === 'function') {
		return Object.getPrototypeOf(obj)
	}
	return obj.__proto__
}
```
# Prototype

## 如何override array built in method

什么时候需要进行数组的方法的重载，比如当进行数据响应式的处理时

```js
// https://www.404forest.com/2017/06/28/modern-web-development-tech-analysis-data-reactivity-system/
// 数组响应化
function observifyArray(arr) {
  const aryMethods = ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse'] //需要变异的函数名列表
  let arrayAugmentations = Object.create(Array.prototype) // 创建一个 __proto__ 到 Array.prototype 的 arrayAugmentations 对象
  aryMethods.forEach(method => {    // 在 arrayAugmentations 对象上将需要变异的函数重写
    arrayAugmentations[method] = function(...arg) {
      Array.prototype[method].apply(this, arg)  // 执行默认操作
      globalDep.notify()  // 重写后的函数会先执行默认操作，随后通知 Dep
    }
  })
  Object.setPrototypeOf(arr, arrayAugmentations)  // 将要监测的数组的原型对象设置为 arrayAugmentations 对象，这样执行 push 等方法时就会执行我们替换后的变异方法啦
}
```
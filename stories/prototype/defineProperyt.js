const obj = {}

Object.defineProperty(obj, 'a', {
  get() {
    console.log('access a')
    return this._value
  },
  set(newValue) {
    console.log('set a')
    this._value = newValue
  }
})

console.log('obj a ', obj.a)
obj.a = 3
console.log('obj a value ', obj.a)
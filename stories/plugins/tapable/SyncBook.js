import React from 'react'
// import SyncHook from './lib/SyncHook'
const SyncHook = require('./lib/SyncHook')

console.log('syn 2c : ', SyncHook)
const util = require("util");
console.log('util : ', util)

class Sync extends React.PureComponent {
  constructor() {
    this.hooks = {
			accelerate: new SyncHook(["newSpeed"]),
    }
  }

  render() {
    return null
  }
}

export default Sync
import React from 'react';

import { storiesOf } from '@storybook/react';

import SyncBook from './tapable/SyncBook'

storiesOf('/plugins/tapable', module)
  .add('SyncBook', () => <SyncBook />)

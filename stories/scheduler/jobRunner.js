const list = new Array(100).fill(0)

function randomIntFromInterval(min, max) { // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const p = () => {
  const timeout = randomIntFromInterval(200, 1000)
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, timeout)
  })
}

const runner = (list, bufferCount, wrappedPromise) => {
  const jobs = []
  let running = 0
  const currentRunning = []
  let currentLastRunningIndex = 0

  const createToken = (item, index) => {
    const pJob = () => new Promise((resolve) => {
      wrappedPromise(index).then(() => {
        resolve()
      })
    })

    return {
      run() {
        running = running + 1
        currentLastRunningIndex = index
        currentRunning.push(index)

        console.log('running : ', {
          running: index,
          currentRunning,
        })

        pJob().then(() => {
          const indexInQueue = currentRunning.indexOf(index)
          currentRunning.splice(indexInQueue, 1)

          if (running < bufferCount + 1) {
            const token = jobs[currentLastRunningIndex + 1]
            if (token) {
              token.run()
            }
          }

          running = running - 1
        })

        if (index < bufferCount - 1 ) {
          const token = jobs[currentLastRunningIndex + 1]
          if (token) {
            token.run()
          }
        }
      }
    }
  }

  list.forEach((item, index) => {
    jobs.push(createToken(item, index))
  })

  jobs[0].run()
}

runner(list, 5, p)

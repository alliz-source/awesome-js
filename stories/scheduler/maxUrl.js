
	const get = url => new Promise(resolve => {
		setTimeout(() => {
				// console.log(url)
			resolve()
		}, 1000)
	})

	function EventBus() {
		const events = {}

		this.on = (name, cb) => {
			events[name] = events[name] ? events[name].push(cb) : [cb]
		}

		this.emit = name => {
			events[name].forEach(item => item())
		}
	}

	const each = (urls, max, get) => {
		const eventBus = new EventBus()
		let current = 0

		eventBus.on('sent', () => {
			if (current === urls.length) {
				return
			}

			const cur = current

			// console.log(`${cur} sending`)
			get(urls[current]).then(() => {
				console.log(`${cur} sent`)
				eventBus.emit('sent')
			})
			current++
		})

		for(;current < max;current++){
			const cur = current
			// console.log(`${cur} sending`)
			get(urls[current]).then(() => {
				console.log(`${cur} sent`)
				eventBus.emit('sent')
			})
		}
	}

	each(new Array(100).fill(0), 5, get)
	// each([0,1,2,3,4,5,6,7,8,9,10], 2, get)
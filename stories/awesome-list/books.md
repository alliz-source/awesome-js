# books

- [Free JavaScript Books](https://medium.com/level-up-web/free-javascript-books-8383ff79576a)
  - [DOM Enlightenment](http://domenlightenment.com/)
  - [The Modern JavaScript Tutorial](http://javascript.info/)
    - [Event loop: microtasks and macrotasks](http://javascript.info/event-loop)
    - [Proxy and Reflect](http://javascript.info/proxy)

- [Eloquent JavaScript](http://eloquentjavascript.net/) 来自 http://marijnhaverbeke.nl/ github https://github.com/marijnh
Promise.all = (arr) => {
    const result = [];

    return new Promise((resolve, reject) => {
        arr.forEach((item, i) => {
            item.then((data) => {
                result[i] = data;
            }, (reason) => {
                console.log('should reject');
                reject(reason);
            }).then(_ => {
                const next = result.filter( item => item);
                if (next.length === arr.length) {
                    resolve(result);
                }
            });
        });
    });
};


const job1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('run job 1');
        resolve('one');
    }, 1000);
});
const job2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('run job 2');
        // reject('two');
        resolve('two');
    }, 2000);
});
const job3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('run job 3');
        resolve('three');
    }, 1500);
});

const jobs = [job1, job2, job3];

Promise.all(jobs).then(data => console.log('data : ', data));
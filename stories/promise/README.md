# Promise

[promise.js](https://gist.github.com/Rich-Harris/11010768)

## when

[when - A solid, fast Promises/A+ and when() implementation, plus other async goodies.](https://github.com/cujojs/when)
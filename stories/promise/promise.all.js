const job1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('run job 1');
        resolve('one');
    }, 1000);
});
const job2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('run job 2');
        reject('two');
    }, 2000);
});
const job3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('run job 3');
        resolve('three');
    }, 1500);
});

const jobs = [job1, job2, job3];

Promise.all(jobs).then((arr) => {
    console.log('arr : ', arr);
}).catch((err) => {
    console.log('err', err);
});

Promise.reject('hello').then((data) => {
    return `${data} - resolve`;
}, (data) => {
    return `${data} - reject`;
}).then((data) => {
    console.log('in resolve : ', data);
}, (data) => {
    console.log('in reject : ', data);
})

Promise.resolve(Promise.reject('test')).then((data) => {
    console.log('data in resolve : ', data);
}, data => {
    console.log('data in reject', data);
})
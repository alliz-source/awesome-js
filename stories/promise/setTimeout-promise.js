console.log('first');

setTimeout(() => {
    console.log('third');

    Promise.resolve('forth').then((data) => {
        console.log(data);
    })
}, 1000);

console.log('second');

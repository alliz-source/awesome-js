# automation

- [How to automate versioning and publication of an npm package](https://itnext.io/how-to-automate-versioning-and-publication-of-an-npm-package-233e8757a526)
- [semantic-release](https://github.com/semantic-release/semantic-release)
- [Conventional Commits 1.0.0](https://www.conventionalcommits.org/en/v1.0.0/)
- [npm scripts](https://docs.npmjs.com/misc/scripts)
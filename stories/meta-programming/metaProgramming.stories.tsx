import React from 'react';

import { storiesOf } from '@storybook/react';

import DependencyValue from './proxy/DependencyValue'
import ReactiveState from './reactive-state'

import ES5Array from './proxy/ES5Array'
import ES5ToObject from './proxy/ES5ToObject'
import ES5Shim from './proxy/ES5Shim'
import ProxyToDefineProperty from './proxy/ProxyToDefineProperty'

storiesOf('/meta-programming/proxy', module)
  .add('dependency-value', () => <DependencyValue />)
  .add('reactive-state', () => <ReactiveState />)
  .add('es5-array', () => <ES5Array />)
  .add('es5-to-object', () => <ES5ToObject />)
  .add('es5-shim', () => <ES5Shim />)
  .add('proxy-to-define-property', () => <ProxyToDefineProperty />)



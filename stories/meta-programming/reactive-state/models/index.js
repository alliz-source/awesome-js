import init from './init'
import goods from './goods'
import bottomBar from './bottomBar'

export default {
  init,
  goods,
  bottomBar,
}

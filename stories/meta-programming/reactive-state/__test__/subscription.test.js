// const {a,b: {f: { g }}} =state
// const { c,d } = a

/**
 * { type: 'get', property: 'a' }
 * { type: 'get', property: 'b' }
 * { type: 'get', property: 'f' }
 * { type: 'get', property: 'g' }
 * { type: 'get', property: 'c' }
 * { type: 'get', property: 'd' }
 */
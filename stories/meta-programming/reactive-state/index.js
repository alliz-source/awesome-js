import React from 'react'
import { Provider, createStore } from './relinx'
import models from './models'
import App from './views'

const store = createStore({
  models
})

export default () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
}
import React from 'react'

export default () => {
  const address = () => {
    return {
      location: 'beijing',
    }
  }

  const user = getState => {
    const { address } = getState()

    return {
      age: 10,
      location: address.location
    }
  }

  let state = new Proxy({
    address: undefined,
    user: undefined,
  }, createHandler())

  state.user = user(() => state)
  state.address = address(() => state)

  console.log('state : ', state, state.user)

  return null
}

function subscription() {
  const listeners = []

  return {
    notify() {
      listeners.forEach(listener => listener())
    },

    subscribe(listener) {
      listeners.push(listener)

      return {
        unsubscribe() {
          const index = listeners.indexOf(listener)
          listeners.splice(index, 1)
        }
      }
    }
  }
}

const createHandler = () => {
  return {
    get(target, property, receiver) {
      // console.log('property  : ', target, property)
      if (!target[property]) {
        const value = new Proxy({}, {
          get(target, property, receiver) {
            console.log('property : ', receiver)
            return 2
            return Reflect.get(target, property, receiver)
          }
        })
        Reflect.set(target, property, value, receiver)
        return value
      }
      return Reflect.get(target, property)
    },
    set() {
      return Reflect.set(...arguments);
    }
  }
}

const createProxy = () => {
  const listenerCollection = new subscription()



}
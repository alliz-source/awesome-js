# Proxy

## lib

- [proxy-method-missing](https://github.com/keithamus/proxy-method-missing)
- [proxy-hide-properties](https://github.com/keithamus/proxy-hide-properties)

## References

- [Metaprogramming with proxies ](https://exploringjs.com/es6/ch_proxies.html)
- [Proxy and Reflect](https://javascript.info/proxy)其中对`receiver`的含义有很好解释
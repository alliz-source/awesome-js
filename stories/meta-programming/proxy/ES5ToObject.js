export default () => {
	const toString = Function.call.bind(Object.prototype.toString)

	const isObject = obj => toString(obj) === '[object Object]'
	const isArray = obj => Array.isArray(obj)

	function each(obj, iter) {
		if (isObject(obj)) {
			Reflect.ownKeys(obj).forEach(key => iter(key, obj[key], obj))
		} else {
			obj.forEach((entry, index) => iter(index, entry, obj))
		}
	}

	const descriptors = {}

	function proxyProperty(
		draft,
		prop,
		enumerable,
	) {
		let desc = descriptors[prop]
		if (desc) {
			desc.enumerable = enumerable
		} else {
			descriptors[prop] = desc = {
				configurable: true,
				enumerable,
				get() {
					console.log('get ', prop)
					return target.__polyfill__[prop]
					// return get(this[DRAFT_STATE], prop)
				},
				set(value) {
					console.log('set prop ', prop)
					target.__polyfill__[prop] = value
					// set(this[DRAFT_STATE], prop, value)
				}
			}
		}

		Object.defineProperty(draft, prop, desc)
	}

	const arr = [{
		a: 1,
	}, {
		b: 2
	}]
	const target = arr.slice()
	Object.defineProperty(target, '__polyfill__', {
		writable: true,
		value: arr.slice(),
		enumerable: false,
	})

	// Object.defineProperty(target, 'length', {
	// 	get() {
	// 		return this.__polyfill__.length
	// 	},
	// 	set(value) {
	// 		this.__polyfill__.length = value
	// 	},
	// 	enumerable: false,
	// })

	each(arr, index => {
		proxyProperty(target, index, true)
	})

	console.log('target ', target)

	arr.forEach(item => console.log('item ', item))

  console.log('arr length ', arr.length)


	return null
}
// 主要为了看当是一个Proxy对象时，调用比如prototype方法到底会触发那些getter

export default () => {
  const arr = [{ a: 1 }, { b: 2 }]
  const emptyFunction = () => {}
  let paths = []

  const proxyArray = new Proxy(arr, {
    get(target, prop, receiver) {
      paths.push(prop)
      return Reflect.get(target, prop, receiver)
    },
    set(target, prop, value, receiver) {
      // console.log('set ', prop)
      return Reflect.set(target, prop, value, receiver)
    }
  })

  const descriptors = Object.getPrototypeOf([])
  const keys = Object.getOwnPropertyNames(descriptors)

  const handler = (func, context, ...rest) => {
    if (paths.length) console.log(func.name, paths)
    paths = []
    func.call(context, ...rest)
  }

  keys.forEach(key => {
    const value = descriptors[key]
    if (typeof value === 'function') {
      let args = []
      //
      if (key === 'concat') handler(value, proxyArray, [])
      if (key === 'copyWith') handler(value, proxyArray, [])

      // fill ["constructor", Symbol(Symbol.isConcatSpreadable), "length", "0", "1"]
      if (key === 'fill') handler(value, proxyArray, [])

      // ["length"]
      if (key === 'find') handler(value, proxyArray, emptyFunction)

      // ["length", "0", "1"]
      if (key === 'findIndex') handler(value, proxyArray, emptyFunction)

      // ["length", "0", "1"]
      if (key === 'lastIndexOf') handler(value, proxyArray, emptyFunction)

      // ["length", "1", "0"]
      if (key === 'pop') handler(value, proxyArray)

      // ["length", "1"]
      if (key === 'push') handler(value, proxyArray)

      // ["length"]
      if (key === 'reverse') handler(value, proxyArray)

      // ["length"]
      if (key === 'shift') handler(value, proxyArray)

      // ["length", "0"]
      if (key === 'unshift') handler(value, proxyArray)

      // ["length"]
      if (key === 'slice') handler(value, proxyArray)

      //  ["length", "constructor"]
      if (key === 'sort') handler(value, proxyArray)

      // ["length"]
      if (key === 'splice') handler(value, proxyArray, 1, 1)

      // ["length", "constructor"]
      if (key === 'includes') handler(value, proxyArray)

      // ["length"]
      if (key === 'indexOf') handler(value, proxyArray)

      // ["length"]
      if (key === 'join') handler(value, proxyArray)

      // ["length"]
      if (key === 'keys') handler(value, proxyArray)

      // ["length"]
      if (key === 'entries') handler(value, proxyArray)

      // ["length", "constructor"]
      if (key === 'forEach') handler(value, proxyArray, emptyFunction)

      // ["length", "constructor"]
      if (key === 'filter') handler(value, proxyArray, emptyFunction)
      if (key === 'flat') handler(value, proxyArray)
      if (key === 'flatMap') handler(value, proxyArray, emptyFunction)

      // ["length", "constructor"]
      if (key === 'map') handler(value, proxyArray, emptyFunction)

      // ["length", "constructor"]
      if (key === 'every') handler(value, proxyArray, emptyFunction)

      // ["length"]
      if (key === 'some') handler(value, proxyArray, emptyFunction)

      // ["length"]
      if (key === 'reduce') handler(value, proxyArray, emptyFunction, {})

      // ["length"]
      if (key === 'reduceRight') handler(value, proxyArray, emptyFunction, {})
    }
  })
  console.log('keys : ', keys)
  return null
}
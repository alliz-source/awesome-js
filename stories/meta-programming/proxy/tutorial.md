# Proxy tutorial

## How to understand `receiver`

> Receiver - Either the proxy or an object that inherits from the proxy.

[Proxy and Reflect](https://javascript.info/proxy) 并且这点在`observer-utils`中有引用到 https://github.com/nx-js/observer-util/blob/master/src/handlers.js#L71

## Issues

### `Cannot create proxy with a non-object as target or handler`

> new Proxy(target, handler)

其中`target`和`handler`都必须是一个对象；否则会报错

## enhance

- [3 ways to use ES6 proxies to enhance your objects](https://blog.logrocket.com/use-es6-proxies-to-enhance-your-objects/)比较吸引人的是第三个，`trap function name`实现函数的扩展

## References
- [Understanding JavaScript Proxies by Examining on-change Library](https://codeburst.io/understanding-javascript-proxies-by-examining-on-change-library-f252eddf76c2)
- [Meta programming with ECMAScript 6 proxies](https://2ality.com/2014/12/es6-proxies.html)
- [Understanding Proxy — Metaprogramming in JavaScript](https://medium.com/@paulrohan/understanding-proxy-metaprogramming-in-javascript-b1c727b747f2)
- [More ES6 Proxy Traps in Depth](https://ponyfoo.com/articles/more-es6-proxy-traps-in-depth)
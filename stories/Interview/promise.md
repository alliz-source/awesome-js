## Promise.all的allSettled 版本

提供一个方法方法实现类似`Promise.all`的功能，但是确保其中一个job失败，仍旧触发then onFulfilled 方法

```js
const allSettledPromiseAll = jobs => {

}

const jobs = [
  Promise.reject(new Error('failed')),
  Promise.resolve(3)
]

allSettledPromiseAll().then((result) => {
  console.log('result ', result)  // result: [{ success: false, result: error }, { success: true, result: 3 }]
})
```

## promise.all polyfill
请写出`promise.all`的polyfill
1. 如果返回值是一个thenable的化，`Promise.resolve`其实是可以一直获取直到是一个value或者error

```js
Promise.resolve(Promise.resolve(2).then(() => Promise.reject(3))).then(value => console.log('va', value), (err) => console.log('xx', err))

// output: xx 3
```
<!-- https://github.com/taylorhakes/promise-polyfill/blob/master/src/index.js#L166 -->

```js
Promise.all = function(arr) {
  return new Promise(function(resolve, reject) {
    if (!isArray(arr)) {
      return reject(new TypeError('Promise.all accepts an array'));
    }

    var args = Array.prototype.slice.call(arr);
    if (args.length === 0) return resolve([]);
    var remaining = args.length;

    function res(i, val) {
      try {
        if (val && (typeof val === 'object' || typeof val === 'function')) {
          var then = val.then;
          if (typeof then === 'function') {
            then.call(
              val,
              function(val) {
                res(i, val);
              },
              reject
            );
            return;
          }
        }
        args[i] = val;
        if (--remaining === 0) {
          resolve(args);
        }
      } catch (ex) {
        reject(ex);
      }
    }

    for (var i = 0; i < args.length; i++) {
      res(i, args[i]);
    }
  });
};
```

## task queue

对n个异步任务进行顺序执行，一次最多运行5个，在最后一个job执行完抛出结束事件

```js
const count = 100
const tasks = []
const randomTimeout = Math.floor(Math.random() * 100)
for (let i = 0; i < 100; i++) {
  tasks.push(() => {
    return new Promise(resolve => {
      setTimeout(() => resolve, randomTimeout)
    })
  })
}

/**
 * tasks: Array<Function> 异步操作的数组
 * limit: Number 每次最多执行的个数
 * callback: Function, `callback` 在最后一个job结束的时候被调用
 **/
const runTask = (tasks, limit, callback) => {
  // ...
}
```


# css

## 盒模型

- [前端面试之盒模型](https://juejin.im/post/5c6d6c566fb9a049aa6faba7)
- [CSS盒模型详解](https://juejin.im/post/59ef72f5f265da4320026f76)

## BFC

- [BFC（Block Formatting Contexts）](https://juejin.im/post/5e6afcc9e51d45270f52d462) 块级格式化上下文；
- [Understanding Block Formatting Contexts in CSS](https://www.sitepoint.com/understanding-block-formatting-contexts-in-css/) 它的中文翻译[理解CSS中BFC](https://www.w3cplus.com/css/understanding-block-formatting-contexts-in-css.html)
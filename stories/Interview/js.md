# JS

## fy

### curry

```js
const curry = (fn, ...args) => (...newArgs) => fn(...args, ...newArgs)
```

## event loop

### throttle

### debounce

### raf
```js
// https://gist.github.com/paulirish/1579671
// https://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
```

### requestIdleCallback

```js
// https://developers.google.com/web/updates/2015/08/using-requestidlecallback

window.requestIdleCallback =
  window.requestIdleCallback ||
  function (cb) {
    var start = Date.now();
    return setTimeout(function () {
      cb({
        didTimeout: false,
        timeRemaining: function () {
          return Math.max(0, 50 - (Date.now() - start));
        }
      });
    }, 1);
  }

window.cancelIdleCallback =
  window.cancelIdleCallback ||
  function (id) {
    clearTimeout(id);
  }
```

### Promise asap

### setTimeout, setInterval, setImmediately, Promise

## Promise

### Promise error是否可以被catch
[Catching Errors in JavaScript Promises with a First Level try … catch](https://stackoverflow.com/questions/24977516/catching-errors-in-javascript-promises-with-a-first-level-try-catch)因为`Promise`是一个异步操作，而`try...catch`是一个同步；所以你是没有办法进行捕获的。但是如果你使用`async`或`await`的话，就可以被正常捕获到了。

### Promise like

```js
// https://github.com/umijs/umi/blob/master/packages/runtime/src/Plugin/Plugin.ts#L26
function isPromiseLike(obj: any) {
  return !!obj && typeof obj === 'object' && typeof obj.then === 'function';
}
```

### promisify
- [promisify](https://javascript.info/promisify)

```js
const promisify = fn => (...args) => {
  return new Promise((resolve, reject) => {
    const next = (reason, ...value) => {
      if (reason) reject(reason)
      else resolve(value)
    }
    fn.apply(null, [...args, next])
  })
}
```

### promise.all

```js
const all = arr => {
  let remaining = arr.length - 1
  return new Promise((resolve, reject) => {
    const value = new Array(arr.length).fill(null)

    const next = (result , index) => {
      if (!!result && typeof result === 'object' && typeof result.then === 'function') {
        result.then(result => {
          next(result, index)
        }, reason => {
          next(reason, index )
        })
      } else {
        if (result instanceof Error) {
          reject(result)
        } else {
          remaining--
          value[index] = result
          if (!remaining) resolve(value)
        }
      }
    }

    arr.map(job => {
      const result = job()
      next(result, index)
    })
  })
}
```

### Promise.race

```js
const race = arr => new Promise((resolve, reject) => {
  const next = task => {
    if (typeof task === 'object' && typeof task.then === 'function') {
      task.then(result => resolve(result), reason => reject(reason))
    } else {
      if (result instanceof Error) reject(result)
      else resolve(result)
    }
  }
  arr.forEach(task => {
    const result = task()
    next(result)
  })
})
```

### cancellable Promise

```js
```

### Promise.allSettle

React的key有什么作用。 介绍一下diff算法。（其实key还有一个dom复用的作用在很多的文章中都没有指出）
实现第四问中的组件chilren diff中的得到patch数组方法，只需要返回插入、删除、更新的节点即可。（这里我们约定了可以直接使用isShallowEqual方法）

## 面经

[字节跳动前端实习面经(1+2+3面+hr）](https://www.nowcoder.com/discuss/373429?type=2&order=0&pos=13&page=1%EF%BC%88%E5%AE%9E%E4%B9%A0%EF%BC%89)
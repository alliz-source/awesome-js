# network

## 网路模型
[网络分层TCP/IP 与HTTP](https://juejin.im/post/5a98e1f7f265da237410694e)

OSI(Open Systems Interconncection)它主要分为7层
  - 应用层
  - 表示层
  - 会话层
  - 传输层
  - 网络层
  - 数据链路层
  - 物理层

TCP/IP模型分为四层：应用层（Application）、传输层（Host-to-Host Transport）、互联网层(Internet)、网络接口层(Network Interface)。在TCP/IP模型中并不包含物理层。另外，两个重要的协议ARP（Address Resolution Protocol，地址解析协议）和RARP（Reverse Address Resolution Protocol，反向地址转换协议），在OSI模型中一般被认为是在位于第二层数据链路层和第三层网络层之间，而在TCP/IP模型中则位于网络接口层

## TCP vs UDP
[TCP/IP协议-为什么说TCP是可靠连接](https://blog.csdn.net/baidu_35692628/article/details/78255476)

### TCP
#### 缺点
1. 三次握手四次挥手，传输更多包，浪费一些带宽
2. 为了进行可靠通信，双方都要维持在线，通信过程中服务器server可能出现非常大的并发连接，浪费了系统资源，甚至会出现宕机
3. 确认重传也会浪费一些带宽，且在不好的网络中，会不断的断开和连接，降低了传输效率

### UDP
#### 优点
1. 没有握手，起步快延时小
2. 不需要维持双方在线，server不用维护巨量并发连接，节省了系统资源
3. 没有重传机制，在不影响使用的情况下，能更高效的利用网络带宽

### TCP为什么是可靠的
1. 确认和重传机制
  1. 建立连接时三次握手同步双方的“序列号 + 确认号 + 窗口大小信息”，是确认重传、流控的基础
  2. 传输过程中，如果Checksum校验失败、丢包或延时，发送端重传
2. 数据排序
  1. TCP有专门的序列号SN字段，可提供数据re-order
3. 流量控制
  1. 窗口和计时器的使用。TCP窗口中会指明双方能够发送接收的最大数据量
4. 拥塞控制
TCP的拥塞控制由4个核心算法组成。
“慢启动”（Slow Start）
“拥塞避免”（Congestion avoidance）
“快速重传 ”（Fast Retransmit）
“快速恢复”（Fast Recovery）
## CORS

- [Cross-Origin Resource Sharing (CORS)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)
- [浏览器同源政策及其规避方法](https://www.ruanyifeng.com/blog/2016/04/same-origin-policy.html)
- [跨域资源共享 CORS 详解](http://www.ruanyifeng.com/blog/2016/04/cors.html)
- [面筋系列-http-同源策略和跨域处理](https://juejin.im/post/5e6b65106fb9a07cd443d08f)

### 什么情况下会有跨域问题

比如前端页面`https://domain-a.com`发送了一个fetch请求到`https://domain-b.com/data.json.`这个时候就涉及到了跨域的问题，因为为了安全的问题比如XMLHttpRequest还有`fetchAPI`遵从同源策略，也就是一个web应用只能够从相同域名下进行请求resource，除非在response中被设置了正确的`CORS headers.`

### 什么是同源策略
1. 协议相同
2. 域名相同
3. 端口相同

### 哪些受同源策略影响
1. Cookie、LocalStorage 和 IndexDB 无法读取。
2. DOM 无法获得。这个主要是针对iframe，如果两个网页不同源，就不能够拿另一个的DOM
3. AJAX 请求不能发送。

### 如何解决AJAX问题

#### JSONP（JSON with Padding（填充））

因为script标签不受同源的限制；它的机制是通过在document在插入一个script标签，标签的src是一个`url+callback`形式，当结果返回以后，会调用callback指定的函数。同时注意返回的结果是一个JSON形式，所以不需要进行再JSON.parse

```js
function addScriptTag(src) {
  var script = document.createElement('script');
  script.setAttribute("type","text/javascript");
  script.src = src;
  document.body.appendChild(script);
}

window.onload = function () {
  addScriptTag('http://example.com/ip?callback=foo');
}

function foo(data) {
  console.log('Your public IP address is: ' + data.ip);
};
```

#### CORS

接下来这个才是这一部分的重点。跨域资源共享，它的解决方式

1. 比如首先会预发一个请求`preflight`，然后获取`approval`以后再进行下面的操作
2. 然后浏览器会有下面的各种乱七八糟的设置，这个时候就需要通过`使用Nginx`来进行实现了。

服务端进行指定，那个域名下可以访问这个资源；同时浏览器会"preflight" the request, soliciting supported methods from the server with the HTTP OPTIONS request method；and then, upon "approval" from the server, sending the actual request浏览器会预发一个请求试探一下，支持的东西，然后验证通过了，才进行对应请求的发送。。

#### Access-Control-Allow-Origin
#### Access-Control-Request-Method

##### nginx

> 反向代理的原理很简单，即所有客户端的请求都必须先经过nginx的处理，nginx作为代理服务器再讲请求转发给node或者java服务，这样就规避了同源策略。

## session和cookie的区别
[Cookie 与 Session 的区别](https://juejin.im/entry/5766c29d6be3ff006a31b84e)
1. cookie是存储到客户端的，可以设置过期时间，以及path之类的信息
2. session是来自服务端为了识别用户的一种机制，它的存储一般都是通过cookie的；比较常见的做法是在cookie中指定一个`sid`字段来进行存储；但是有的时候cookie会被禁止掉，所以这个时候的一个做法是URL重写就是将sid直接追加到URL路径的后面。还有一种技术叫做表单隐藏字段。就是服务器会自动修改表单，添加一个隐藏字段，以便在表单提交时能够把session id传递回服务器。

### 存储方式的不同
Cookie中只能保管ASCII字符串，假如需求存取Unicode字符或者二进制数据，需求先进行编码。Cookie中也不能直接存取Java对象。若要存储略微复杂的信息，运用Cookie是比拟艰难的。

而Session中能够存取任何类型的数据，包括而不限于String、Integer、List、Map等。Session中也能够直接保管Java Bean乃至任何Java类，对象等，运用起来十分便当。能够把Session看做是一个Java容器类。

### 服务器压力的不同

session是保存在服务端的，而cookie是在客户端的，所以cookie是不会占用服务器资源的。

### 浏览器支持不同

Cookie是需要客户端浏览器支持的。假如客户端禁用了Cookie，或者不支持Cookie，则会话跟踪会失效。关于WAP上的应用，常规的Cookie就派不上用场了。

假如客户端浏览器不支持Cookie，需要运用Session以及URL地址重写。需要注意的是一切的用到Session程序的URL都要进行URL地址重写，否则Session会话跟踪还会失效。关于WAP应用来说，Session+URL地址重写或许是它唯一的选择。

假如客户端支持Cookie，则Cookie既能够设为本浏览器窗口以及子窗口内有效（把过期时间设为–1），也能够设为一切阅读器窗口内有效（把过期时间设为某个大于0的整数）。但Session只能在本阅读器窗口以及其子窗口内有效。假如两个浏览器窗口互不相干，它们将运用两个不同的Session。（IE8下不同窗口Session相干）

### 跨域支持上的不同

cookie的话，如果设置好Path的话，那么path下的域名都是可以访问的；但是session的话，只有它所在域名下访问才有效

## HTTP status code

1. 1xx Informational response
2. 2xx Success
3. 3xx Redirection
4. 4xx Client errors
5. 5xx Server errors

## GET, POST, PUT区别

1. [When should I use GET or POST method? What's the difference between them?](https://stackoverflow.com/questions/504947/when-should-i-use-get-or-post-method-whats-the-difference-between-them)
2. [What is the difference between POST and GET?](https://stackoverflow.com/questions/3477333/what-is-the-difference-between-post-and-get)

> GET requests a representation of the specified resource. Note that GET should not be used for operations that cause side-effects, such as using it for taking actions in web applications. One reason for this is that GET may be used arbitrarily by robots or crawlers, which should not need to consider the side effects that a request should cause.
> POST submits data to be processed (e.g., from an HTML form) to the identified resource. The data is included in the body of the request. This may result in the creation of a new resource or the updates of existing resources or both.

get主要是用来获取展示内容，不包含side-effect（比如用户提交一个东西，然后接下来会影响页面的展示这个就可以认为是一个side effect）；也就是说涉及到获取data的操作，不能够用get，

> Authors of services which use the HTTP protocol SHOULD NOT use GET based forms for the submission of sensitive data, because this will cause this data to be encoded in the Request-URI. Many existing servers, proxies, and user agents will log the request URI in some place where it might be visible to third parties. Servers can use POST-based form submission instead

然后因为如果get请求上带了一些敏感的信息的话，在后端日志上它会留有记录，这个是很危险的操作。同时在一些浏览器上还会对get数据进行缓存，比如即使你后端返回的是不同的结果，但是你前端拿到的依旧是同一个结果；避免这种情况，可以通过追加timestamp来实现，但这个终归不是一个好的方式。最后在内容的长度上，同时是有区别的，get会有size的约束。。。

1. GET when you want to retrieve data (GET DATA).
2. POST when you want to send data (POST DATA).

## HTTP2

- [HTTP----HTTP2.0新特性](https://juejin.im/post/5a4dfb2ef265da43305ee2d0)
- [如何理解http2的多路复用（Multiplexing）（两个小问题）？](https://segmentfault.com/q/1010000005167289)

在HTTP1.1中，浏览器客户端在同一时间，针对同一域名下的请求有一定数量的限制。超过限制数目的请求会被阻塞。而HTTP2.0中的多路复用优化了这一性能。

### 什么是多路复用

基于二进制分帧层，HTTP2.0可以在共享TCP链接的基础上同时发送请求和响应。HTTP消息被分解为独立的帧，而不破坏消息本身的语义，交错发出去，在另一端根据流标识符和首部将他们重新组装起来。

### 服务器推送对性能优化工作的贡献

用户首先发了一个请求以后，他可以返回多个response，比如你请求一个页面的同时会将其中的css，js同时发送给你，因为它知道客户端需要这些东西。这样不但减轻了数据传送冗余步骤，也加快了页面响应的速度，提高了用户体验。

## 缓存机制

[HTTP----HTTP缓存机制](https://juejin.im/post/5a1d4e546fb9a0450f21af23)

### 强制缓存

主要是通过Expires和Cache-Control这两个字段来进行控制的，

### 协商缓存

1. Last-Modified： 服务器在响应请求时，会告诉浏览器资源的最后修改时间。
2. if-Modified-Since: 浏览器再次请求服务器的时候，请求头会包含此字段，后面跟着在缓存中获得的最后修改时间；如果真的修改了就返回200，同时返回新的文件；如果没有就返回304
3. if-Unmodified-Since: 从字面上看, 就是说: 从某个时间点算起, 是否文件没有被修改
4. Etag： 服务器响应请求时，通过此字段告诉浏览器当前资源在服务器生成的唯一标识（生成规则由服务器决定）但是这个东西需要依赖算法，消耗服务端资源，所以目前就不怎么用了


## QA
https://www.nowcoder.com/discuss/368464?type=2
1. 从输入域名到页面展现之间发生了什么。（一直以来非常经典有非常多知识点的问题，需要指出的是大多数文章并没有提到与 dns-prefetch preload prefetch 相关的点。）
2. 接第八点提到了缓存策略，于是要求简单介绍一下浏览器缓存策略。
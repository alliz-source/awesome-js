// const curry = (fn, ...args) => (...newArgs) => fn(...args, ...newArgs)

const sub = (a, b, c) => {
  return a + b + c
}

function curry(fn, ...args) {
  return fn.length === args.length ? fn.apply(null, args) : (...nextArgs) => curry.apply(null, [fn, ...args, ...nextArgs])
}

function sum(x, y, z) {
  return x + y + z
}

const partialSum = curry(sum)
const fnAdd1 = partialSum(1)
// const fnAdd2 = partialSum(2)
const fnAdd1Add3 = fnAdd1(3)
console.log('total ', fnAdd1Add3(5))

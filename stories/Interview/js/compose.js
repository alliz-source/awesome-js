function compose(...funcs) {
  return (...args) => funcs.reduce((a, b) => {
    console.log('a ', a)
    return b(a)
  }, ...args)
}

const f = i => i + 1
const h = i => i + 4
const g = i => i + 5

console.log(compose(f, h, g)(7))


// https://github.com/reduxjs/redux/blob/master/src/compose.ts
function composeRedux(...funcs) {
  if (funcs.length === 0) {
    return arg => arg
  }

  if (funcs.length === 1) {
    return funcs[0]
  }

  return funcs.reduce((a, b) => (...args) => a(b(...args)))
}

// 对比`compose`和`composeRedux`的话，他们的返回策略其实是不一样的；`compose`的话，直接返回了函数，这个来自[Curry and Function Composition](https://medium.com/javascript-scene/curry-and-function-composition-2c208d774983)；其实注意compose的话，它解决的都是实参为1个的情况，要不没有办法搞。。
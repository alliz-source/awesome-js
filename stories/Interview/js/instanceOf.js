function instanceOf(a, b) {
  let proto = getPropertyOf(a)
  console.log('proto ', proto)
	while (proto) {
		if (proto === b.prototype) return true
		proto = getPropertyOf(proto)
	}

	return false
}

const getPropertyOf = obj => {
	if (typeof Object.getPrototypeOf === 'function') {
		return Object.getPrototypeOf(obj)
	}
	return obj.__proto__
}

function state(){}

state.prototype.show = function() {
  console.log('show')
}

const value = new state()
const next = new state()

console.log('value instance of state', instanceOf(value, state))
console.log('prototype equal ', value.show === next.show)
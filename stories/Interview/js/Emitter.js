// 需要注意的是once
// [emitter](https://github.com/facebookarchive/emitter/tree/master/src)
// [eventemitter.js - gist](https://gist.github.com/mudge/5830382)

class Emitter {
  constructor() {
    this.subscriptions = {}
  }

  on(eventName, cb) {
    if (!this.subscriptions[eventName]) this.subscriptions[eventName] = []
    this.subscriptions[eventName].push(cb)
    return () => {
      this.removeListener(eventName, cb)
    }
  }

  emit(eventName) {
    const listeners = this.subscriptions[eventName] || []
    listeners.forEach(listener => listener())
  }

  removeListener(eventName, fn) {
    if (this.subscriptions[eventName]) {
      const index = this.subscriptions[eventName].indexOf(cb)
      if (index !== -1) this.subscriptions[eventName].splice(index, 1)
    }
  }

  once(eventName, cb) {
    this.on(eventName, function fn() {
      this.removeListener(eventName, fn)
      cb.apply(this, arguments)
    })
  }

  addListener(eventName, cb) {
    this.on(eventName, cb)
  }
}
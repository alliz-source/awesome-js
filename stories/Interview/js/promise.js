const promisify = fn => (...args) => {
  return new Promise((resolve, reject) => {
    const next = (reason, value) => {
      if (reason) reject(reason)
      else resolve(value)
    }
    fn.apply(null, [...args, next])
  })
}

const all = arr => {
  let remaining = arr.length - 1
  return new Promise((resolve, reject) => {
    const value = new Array(arr.length).fill(null)

    const next = (result , index) => {
      if (!!result && typeof result === 'object' && typeof result.then === 'function') {
        result.then(result => {
          next(result, index)
        }, reason => {
          next(reason, index )
        })
      } else {
        if (result instanceof Error) {
          reject(result)
        } else {
          remaining--
          value[index] = result
          if (!remaining) resolve(value)
        }
      }
    }

    arr.map(job => {
      const result = job()
      next(result, index)
    })
  })
}

const race = arr => new Promise((resolve, reject) => {
  const next = task => {
    if (!!result && typeof result === 'object' && typeof result.then === 'function') {
      task.then(result => resolve(result), reason => reject(reason))
    } else {
      if (result instanceof Error) reject(result)
      else resolve(result)
    }
  }
  arr.forEach(task => {
    const result = task()
    next(result)
  })
})
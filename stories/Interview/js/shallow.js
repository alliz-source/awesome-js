function is(a,b) {
  if (a === b) {
    return x !== 0 || 1 / x === 1 / y
  } else {
    return x !== x && y !== y
  }
}

const toString = Function.call.bind(Function.prototype.toString)

function shallowEqual(a, b) {
  if (is(a, b)) return true
  const typeA = toString(a)
  const typeB = toString(b)

  if (typeA !== typeB) return false

  if (Array.isArray(a)) {
    const lenA = a.length
    const lenB = b.length
    if (lenA !== lenB) return false
    for(let i = 0; i < lenA; i++) {
      if (a[i] !== b[i]) return false
    }
    return true
  }

  if (typeA === '[object Object]') {
    const lenA = Object.keys(a).length
    const lenB = Object.keys(b).length
    if (lenA !== lenB) return false
    for(let i = 0; i < lenA; i++) {
      const key = keys[A]
      if (a[key] !== b[key]) return false
    }
    return true
  }
}

// 上面是我的写法。。。分开了array和object比较尴尬；因为array中的index其实是可以理解为`ownProperty`的
// [Replace 'react-addons-shallow-compare' with first party implementation #1671](https://github.com/airbnb/react-dates/issues/1671)

function shallowCompare(instance, nextProps, nextState) {
	return (
		!shallowEqual(instance.props, nextProps) ||
		!shallowEqual(instance.state, nextState)
	);
}

function shallowEqual(objA, objB) {
	if (is(objA, objB)) {
		return true;
	}

	if (typeof objA !== 'object' || objA === null || typeof objB !== 'object' || objB === null) {
		return false;
	}

	var keysA = Object.keys(objA);
	var keysB = Object.keys(objB);

	if (keysA.length !== keysB.length) {
		return false;
	}

	for (var i = 0; i < keysA.length; i++) {
		if (!hasOwnProperty.call(objB, keysA[i]) || !is(objA[keysA[i]], objB[keysA[i]])) {
			return false;
		}
	}

	return true;
}

function is(x, y) {
	if (x === y) {
		// Steps 1-5, 7-10
		// Steps 6.b-6.e: +0 != -0
		// Added the nonzero y check to make Flow happy, but it is redundant
		return x !== 0 || y !== 0 || 1 / x === 1 / y;
	} else {
		// Step 6.a: NaN == NaN
		return x !== x && y !== y;
	}
}

# array

## remove duplicates
[Remove duplicate values from JS array [duplicate]](https://stackoverflow.com/questions/9229645/remove-duplicate-values-from-js-array)

### Set

```js
function removeDuplicates(arr) {
  return [...new Set(arr)]
}
```

### filter

这个方式是比较讨巧的，也就是说你拿到的第一个index如果跟当前的index不一样的话，证明前面是已经有这个值了

```js
array.filter((item, index) => array.indexOf(item) !== index);
```

### reduce

```js
const arr = []
function removeDuplicates(arr) {
  return arr.reduce((result, cur) => {
    if (result.indexOf(cur) === -1) result.push(cur)
    return result
  }, [])
}
```

## use reduce to implement map

```js
Array.prototype.map = function(iter) {
  const arr = this
  return arr.reduce((result, cur, index) => {
    const value = iter(cur, index)
    result[index] = value
    return result
  }, [])
}

const arr = [2,3,3]
const result = arr.map((value, index) => {
  return value + index
})
```
function throttle(fn, ms = 5000) {
  let isAvailable = true
  return (...args) => {
    if (!isAvailable) return
    isAvailable = false
    setTimeout(() => {
      fn(...args)
      isAvailable = true
    }, ms)
  }
}

function attempt() {
  console.log('run attempt')
}

const throttleFn = throttle(attempt, 5000)

setInterval(() => {
  throttleFn()
}, 1000)
Function.prototype.bind = function(...args) {
  const context = args[0]
  const fn = this
  return function(...nextArgs) {
    return fn.apply(context, [...nextArgs, ...args.slice(1)])
  }
}

function sum(b) {
  const { a } = this
  return a + b
}

const fn = sum.bind({a: 4}, 7)
console.log('value ', fn())
// 正则表达式问题

// 字符串中多个空格，替换为一个

function suppressSpace(str) {
  return str.replace(/\s+/g, ' ')
}

// const str = suppressSpace('ab    xc x c')
// console.log('result ', str)

// 去除字符串前后空格

function trim(str) {
  return str.replace(/(^\s+)|(\s+$)/g, '')
}

// const s = '    xfc  xx '
// const trimmed = trim(s)
// console.log('trimmed ',trimmed)

// 多空格字符串格式化为数组

function toArray(str) {
  const trimmed = trim(str)
  const next = suppressSpace(trimmed)
  const parts = next.split(' ')

  console.log('parts ', parts)
}

console.log(toArray('    xfc  xx '))
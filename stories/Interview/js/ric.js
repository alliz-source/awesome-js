window.requestIdleCallback = (callback) => {
  const startTime = Date.now()
  return setTimeout(() => {
    callback({
      didTimeout: false,
      timeRemaining: function() {
        Date.now() - startTime
      }
    })
  })
}

window.cancelIdleCallback = window.cancelIdleCallback || function(id) {
  clearTimeout(id)
}
# hoist

- [Understanding Hoisting in JavaScript](https://medium.com/zestgeek/understanding-hoisting-in-javascript-3ca061273fff)
- [var vs let vs const in JavaScript](https://tylermcginnis.com/var-let-const/)
- [The Ultimate Guide to Hoisting, Scopes, and Closures in JavaScript](https://tylermcginnis.com/ultimate-guide-to-execution-contexts-hoisting-scopes-and-closures-in-javascript/)
# let和var的区别

```js
for (let i = 0; i < 6; i++) {
  setTimeout(function() {
    console.log(i)
  })
}
```
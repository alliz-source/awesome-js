// https://tylermcginnis.com/ultimate-guide-to-execution-contexts-hoisting-scopes-and-closures-in-javascript/

var obj = {
  fnA() { console.log(this); },
  fnB: () => { console.log(this); },
};

obj.fnA()
obj.fnB()
const { fnA, fnB } = obj;
fnA()
fnB()



// 223233243434.43
// 千分位处理
function transform(number) {
  try {
    const numberString = `${number}`
    return numberString.split('').reverse().reduce((result, cur, index) => {
      console.log('cur ', cur)
      if (index && !(index % 3)) {
        return `${cur},${result}`
      }

      return `${cur}${result}`
    }, '')

  } catch (err) {
    return null
  }
}

const a = 2323432432443
console.log('transform a ', transform(a))


// const b = `${a}`.replace(/(\d{1,3})(?=(\d{3})+$)/g, (match, $1) => {
//   console.log('$1' , $1, match)
//   return `${$1},`
// })

const b = `${a}`.replace(/\d{1,3}(?=(\d{3})+$)/g, (match, $1) => {
  console.log('$1' , $1, match)
  return `${match},`
})

console.log('b ', b)

const c = /\d{1,3}(?=(\d{3})+$)/g.test(a)
// const c = /\d{1,3}(?=\d{3})+$/.test(a)
console.log('c ', c)

const reg = /\d{1,3}(?=(\d{3})+$)/g

let matched
while((matched = reg.exec(`${a}`)) !== null) {
  console.log('matched ', matched)
}

const reg2 = /\d{1,3}(?=(\d{3})+$)/g
const reg4 = /\d{1,3}(?=(\d{3})+$)/g

// console.log('reg ', reg2 === reg4)
const s = `${a}`

s.match(reg2)
(function() {
  const last = 0
  let timeoutHandler
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = callback => {
      const current = Date.now()
      const timeToCall = Math.max(0, 16 - (current - last))
      const timeoutHandler = setTimeout(() => callback(current + timeToCall), timeToCall)
      last = current + timeToCall

      return timeoutHandler
    }

    window.cancelAnimationFrame = (timeoutHandler) => {
      timeoutHandler = 0
      clearTimeout(timeoutHandler)
    }
  }
})()
const fn = n => {
  if (n === 0 || n === 1) return n

  return fn(n-1) + fn(n-2)
}

const result = fn(9)

console.log('result ', result)

const fn2 = n => {
  let a = 1
  let b = 1
  let result

  for (let i = 2; i < n; i++) {
    result = a + b;
    a = b
    b = result
  }

  return result
}

console.log('fn2 9 ', fn2(9))
# sort

## quick sort

[QuickSort Algorithm in JavaScript](https://www.guru99.com/quicksort-in-javascript.html)它是js中的默认排序算法，符合`Divide and Conquer`的规则，也就是分而治之的方式，

1. 首先找到pivot
2. 用数组中的元素和pivot进行比较，小的放左边，大的放右边
3. 然后对左右的都进行同样的和这个操作。

```js
// https://gist.github.com/joelpalmer/beab8d9a2de02c39ae09fea50dab5ea5
const qSort = (arry) => {
    if (arry.length === 0) {
        return [];
    }
    let left = [], right =[], pivot = arry[0];
    for (let i=1; i< arry.length; i++) {
        if(arry[i] < pivot) {
            left.push(arry[i]);
        } else {
            right.push(arry[i]);
        }
    };

    return [...qSort(left), pivot, ...qSort(right)];
}

const g = qSort(["b", "l", "c", "a"]);
```

其实核心就是如何确定pivot，如果说将第一个作为pivot的话，会造成最大的性能损耗；一般情况下都是，用中间位置的元素；注意最后返回的是一个index；最终达到的是，左边都是比一个数值小。

```js
var items = [5,3,7,6,2,9];
function swap(items, leftIndex, rightIndex){
    var temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
}
function partition(items, left, right) {
    var pivot   = items[Math.floor((right + left) / 2)], //middle element
        i       = left, //left pointer
        j       = right; //right pointer
    while (i <= j) {
        while (items[i] < pivot) {
            i++;
        }
        while (items[j] > pivot) {
            j--;
        }
        if (i <= j) {
            swap(items, i, j); //sawpping two elements
            i++;
            j--;
        }
    }
    return i;
}

function quickSort(items, left, right) {
    var index;
    if (items.length > 1) {
        index = partition(items, left, right); //index returned from partition
        if (left < index - 1) { //more elements on the left side of the pivot
            quickSort(items, left, index - 1);
        }
        if (index < right) { //more elements on the right side of the pivot
            quickSort(items, index, right);
        }
    }
    return items;
}
// first call to quick sort
var sortedArray = quickSort(items, 0, items.length - 1);
console.log(sortedArray); //prints [2,3,5,6,7,9]
```
1. n级台阶，从0开始走起，一次可以走一步或者两步，那么走完n级台阶一共有多少种走法？先讲思路再写代码。

## Two sum
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

```js
// https://leetcode.com/problems/two-sum/discuss/293585/javascript-solution-for-two-sum-9946-runtime
var twoSum = function(nums, target) {
    var numObj = new Map();
    for (var i = 0; i < nums.length; i++) {
        numObj.set(nums[i], i);
    }

    for (var i = 0; i < nums.length; i++) {
        var complement = target - nums[i];
        if (numObj.has(complement) && numObj.get(complement) != i) {
            return [i, numObj.get(complement)];
        }
    }
    return [];
};
```

## 09 回文数

Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

```js
/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function(x) {
  const s = String(x)
  for (let i = 0, j = s.length -1; i < j; i++, j--) {
    if (s[i] !== s[j]) {
      return false
    }
  }
  return true
};
```

## 5. Longest Palindromic Substring

[5. Longest Palindromic Substring](https://github.com/BaffinLee/leetcode-javascript/blob/master/001-100/5.%20Longest%20Palindromic%20Substring.md)

### 问题点
1. 是否存在交叉的情况
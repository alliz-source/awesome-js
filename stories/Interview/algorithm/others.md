# other

## Finding the most frequent character in a string javascript

[Find the most commonly used character in string](https://medium.com/@Dragonza/find-the-most-commonly-used-character-in-string-ed00dc2279be)

```js
function maxChar(str) {
  const obj = {};
  str.split('').forEach(char => obj[char] + 1 || 1);
  return Object.keys(obj).reduce((prev, next) => obj[a] >= obj[b] ? a : b);
}
```

## counting frequency
[counting frequency of characters in a string using javascript](https://stackoverflow.com/questions/18619785/counting-frequency-of-characters-in-a-string-using-javascript)

```js
function getFrequency(string) {
    var freq = {};
    for (var i=0; i<string.length;i++) {
        var character = string.charAt(i);
        if (freq[character]) {
           freq[character]++;
        } else {
           freq[character] = 1;
        }
    }

    return freq;
};
```
# reactive data

谈到数据的响应式，其实这也是正是框架`Vue`的核心理念；[Mobx 思想的实现原理，及与 Redux 对比](https://zhuanlan.zhihu.com/p/25585910)中尤大也评论说

> 其实跟 Vue, Knockout, Meteor Tracker 原理是一样的，这套前端用的最早的应该还是 knockout。

对我而言，比较早期的启蒙是 [observer-util](https://github.com/nx-js/observer-util)以及`meteor tracker`

## 状态管理剖析对比

### mobx vs redux

[Mobx 思想的实现原理，及与 Redux 对比](https://zhuanlan.zhihu.com/p/25585910)介绍了如何进行observable的声明，其实核心代码的话，也就是下面的部分；当返回值是一个对象的时候，返回一个Proxy（没有就创建）

```js
const dynamicObject = new Proxy(obj, {
    // ...
    get(target, key, receiver) {
        const result = Reflect.get(target, key, receiver)

        // 如果取的值是对象，优先取代理对象
        const resultIsObject = typeof result === 'object' && result
        const existProxy = resultIsObject && proxies.get(result)

        // 将监听添加到这个 key 上
        if (currentObserver) {
            registerObserver(target, key)
            if (resultIsObject) {
                return existProxy || toObservable(result)
            }
        }

        return existProxy || result
    }),
    // ...
})
```

区别的主要观点是：
1. mobx对于回溯比较麻烦；但是虽然说redux可以回溯，但是成本比较高，需要immutable的支持
2. mobx偏向于小而不复杂的项目；并且因为是纯函数，所以它在TS上支持比较好。

最后推荐了一下，自己的库叫[Light and fast 🚀 state management tool using proxy. https://dobjs.github.io/dob-docs/](https://github.com/dobjs/dob)
阅读了一下实现原理[35.精读《dob - 框架实现》.md](https://github.com/dt-fe/weekly/blob/master/35.%E7%B2%BE%E8%AF%BB%E3%80%8Adob%20-%20%E6%A1%86%E6%9E%B6%E5%AE%9E%E7%8E%B0%E3%80%8B.md#%E6%8A%BD%E4%B8%9D%E5%89%A5%E8%8C%A7%E5%AE%9E%E7%8E%B0%E4%BE%9D%E8%B5%96%E8%BF%BD%E8%B8%AA)，里面提到了一点[为什么依赖追踪只支持同步函数](https://github.com/dt-fe/weekly/blob/master/35.%E7%B2%BE%E8%AF%BB%E3%80%8Adob%20-%20%E6%A1%86%E6%9E%B6%E5%AE%9E%E7%8E%B0%E3%80%8B.md#%E4%B8%BA%E4%BB%80%E4%B9%88%E4%BE%9D%E8%B5%96%E8%BF%BD%E8%B8%AA%E5%8F%AA%E6%94%AF%E6%8C%81%E5%90%8C%E6%AD%A5%E5%87%BD%E6%95%B0)这个其实也是`relinx`在设计时碰到的问题，所以需要通过observe函数包装一下，来进行scope的划分。

## 源码剖析

因为是对`reactive data`的描述；所以，这里面的源码都是关于`Vue`的
1. [vue - core 源码](https://github.com/vuejs/vue/tree/dev/src/core/observer)

2. [现代前端科技解析 —— 数据响应式系统 (Data Reactivity System)](https://www.404forest.com/2017/06/28/modern-web-development-tech-analysis-data-reactivity-system/) 参照Vue的源码进行剖析；它里面值得借鉴的代码

```js
// 使一个对象响应化
function observify(value) {
  if(!isObject(value)) {
    return
  }
  Object.keys(value).forEach((key) => {
    defineReactive(value, key, value[key])  // 遍历每个键使其响应化
  })
}
function isObject(value) {
  return typeof value === 'object' && value !== null
}
//为对象的一个键应用 Object.defineProperty
function defineReactive(obj, key, value) {
  observify(value) // 递归
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get() {
      return value
    },
    set: (newValue) => {
      if(newValue === value) {
        return
      }else {
        let oldValue = value
        value = newValue
        observify(newValue)
        globalDep.notify(newValue, oldValue) // 变动时通知 Dep
      }
    }
  })
}
```
3. [Vue源码详细解析(一)--数据的响应化 #1](https://github.com/Ma63d/vue-analysis/issues/1)其中有一个比较有意思的问题是[let data = {a: {b: {c: {d: {e: 1}}}}}如何向下搜集](https://github.com/Ma63d/vue-analysis/issues/1#issuecomment-310263220)

Vue的解决方式是通过下面的方式，[deep watch](https://github.com/Ma63d/vue-analysis/blob/master/vue%E6%BA%90%E7%A0%81%E6%B3%A8%E9%87%8A%E7%89%88/watcher.js#L125-L129)我比较好奇的点是，为什么会有这个需求，因为本身都是函数，如果说，这个逻辑下没有走到它说明当前的`property`就无关紧要

```js
  // "touch" every property so they are all tracked as
  // dependencies for deep watching
  if (this.deep) {
    traverse(value)
  }
```
# How to write Error

比较常见的一个处理Error的库

## nullthrow

[nullthrow](https://github.com/zertosh/nullthrows)基本结构如下

```js
nullthrows(value)
```

如果说value是一个undefined或者null的话直接报错；否则就返回value的值；其中引入了下面一句

```js
// https://github.com/zertosh/nullthrows/blob/master/nullthrows.js#L8
  error.framesToPop = 1; // Skip nullthrows's own stack frame.
```

没太懂，然后可以参考[Why popular package invariant.js contains this code: error.framesToPop = 1;](https://stackoverflow.com/questions/55374385/why-popular-package-invariant-js-contains-this-code-error-framestopop-1)对于写法framesToTop应该是fb内部的一个机制；同时参考[can anyone please enlighten me on how `error.framesToPop` is used in @reactjs](https://twitter.com/joshwnj/status/578062208283717632?lang=en)

### 使用场景

- [parcel](https://github.com/parcel-bundler/parcel/blob/v2/packages/core/core/src/Parcel.js#L29)

## More

[rollup - ast.nodes](https://github.com/rollup/rollup/tree/master/src/ast/nodes)存在ThrowStatement和TryStatement的解析
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Button } from '@storybook/react/demo';

storiesOf('/EventLoop/basic', module)
  .add('basic', () => <Button onClick={action('clicked')}>Hello Button</Button>)

setInterval(() => {
  Promise.resolve().then(() => console.log('promise'))
}, 1000)

setInterval(function() {
  setTimeout(function() {
      console.log('setTimeout3');
  }, 0);

  setImmediate(function() {
      console.log('setImmediate4');
  });

  process.nextTick(function() {
      console.log('nextTick2');
  });

  console.log('console1');
}, 2000)
# code analysis

## nested microTask

当microTask中嵌套microTask时，它会怎么执行

```js
console.log('start')

Promise.resolve().then(() => {
  console.log('first promise')
  Promise.resolve().then(() => {
    console.log('first nested promise')
  })
})

Promise.resolve().then(() => {
  console.log('second promise')
  Promise.resolve().then(() => {
    console.log('second nested promise')
  })
})

setTimeout(() => {
  console.log('timeout start')
}, 0)

// start
// first promise
// second promise
// first nested promise
// second nested promise
// timeout start
```

```js
let racer1 = function() {
  console.log('race 1')
  setTimeout(() => console.log("timeout 1"), 0);
  setImmediate(() => console.log("immediate 1"));
  Promise.resolve().then(() => console.log('promise 1'))
  requestAnimationFrame(() => console.log('raf 1'))
}

let racer2 = function() {
  console.log('race 2')
  setTimeout(() => console.log("timeout 2"), 0);
  setImmediate(() => console.log("immediate 2"));
  Promise.resolve().then(() => console.log('promise 2'))
  requestAnimationFrame(() => console.log('raf 2'))
}

let racer3 = function() {
  console.log('race 3')
  setImmediate(() => console.log("immediate 3"));
  setTimeout(() => console.log("timeout 3"), 0);
  Promise.resolve().then(() => console.log('promise 3'))
  requestAnimationFrame(() => console.log('raf 3'))
}

racer1()
racer2()
racer3()
```

## setTimeout, setImmediate, process.nextTick

现在比较困惑的是，当存在process.nextTick时，`setTimeout`会比`setImmediate`要早，难道是因为进入了一个新的EventLoop

```js
setImmediate(function(){
    console.log(1);
},0);
setTimeout(function(){
    console.log(2);
},0);
new Promise(function(resolve){
    console.log(3);
    resolve();
    console.log(4);
}).then(function(){
    console.log(5);
});
console.log(6);
process.nextTick(function(){
    console.log(7);
});
console.log(8);


➜  event-loop git:(master) ✗ node test.js
3
4
6
8
7
5
2
1
```
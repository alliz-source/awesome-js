# render with event loop

- [How Browsers Work: Behind the scenes of modern web browsers](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/)
- [对浏览器首次渲染时间点的探究](https://404forest.com/2019/04/23/when-does-the-browser-first-paint/)
- [深入探究 eventloop 与浏览器渲染的时序问题](https://404forest.com/2017/07/18/how-javascript-actually-works-eventloop-and-uirendering/)
- [从 event loop 规范探究 javaScript 异步及浏览器更新渲染时机](https://juejin.im/entry/59082301a22b9d0065f1a186)
- [When is JavaScript synchronous?](https://stackoverflow.com/questions/2035645/when-is-javascript-synchronous)
- [8.1.4 Event loops](https://html.spec.whatwg.org/multipage/webappapis.html#event-loops)官方文档

- [浏览器渲染详细过程：重绘、重排和 composite 只是冰山一角](https://juejin.im/entry/590801780ce46300617c89b8)
- [无线性能优化：Composite](https://fed.taobao.org/blog/2016/04/26/performance-composite/)

## 通过图标来理解

[requestAnimationFrame Scheduling For Nerds](https://medium.com/@paul_irish/requestanimationframe-scheduling-for-nerds-9c57f7438ef4)

![render-1](./assets/render-1.png)
![render-2](./assets/render-2.png)
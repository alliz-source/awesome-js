// setTimeout(function() {
//   setTimeout(function() {
//     console.log('setTimeout')
//   }, 0);
//   setImmediate(function() {
//     console.log('setImmediate')
//   });
// }, 10);

// function test() {
//   setTimeout(function() {
//     console.log('setTimeout')
//   }, 0);
//   setImmediate(function() {
//     console.log('setImmediate')
//   });
// }

// function test1() {
//   setTimeout(function() {
//     console.log('setTimeout 1')
//   }, 0);
//   setImmediate(function() {
//     console.log('setImmediate 1')
//   });
// }


// test()
// test1()

// let racer1 = function() {
//   setTimeout(() => console.log("timeout 1"), 0);
//   setImmediate(() => console.log("immediate 1"));
//   process.nextTick(() => console.log("nextTick"));
// }

// let racer2 = function() {
//   // process.nextTick(() => console.log("nextTick"));
//   // Promise.resolve().then(() => console.log('promise'))
//   setTimeout(() => console.log("timeout 2"), 0);
//   setImmediate(() => console.log("immediate 2"));
// }

// let racer3 = function() {
//   setImmediate(() => console.log("immediate 3"));
//   // process.nextTick(() => console.log("nextTick"));
//   setTimeout(() => console.log("timeout 3"), 0);
// }

// racer1()
// racer2()
// racer3()



// function cb(arg) {
//   return function() {
//     console.log(arg);
//     process.nextTick(function() {
//       console.log('nextTick - ' + arg);
//     });
//   }
// }

// cb('0')();

// process.nextTick(function() {
//   console.log('nextTick outer');
// });

// function fn() {
//   process.nextTick(function() {
//     console.log('nextTick inner');
//   });
//   // setTimeout(cb('3'), 0)
//   // setImmediate(cb('1'));
//   // setImmediate(cb('2'));

//   // setTimeout(() => {
//   //   console.log('xxx')
//   // }, 0)
//   setTimeout(() => console.log('timeout 1'), 0)
//   setTimeout(() => console.log('timeout 2'), 0)

//   setImmediate(() => console.log('immediate 1'))
//   setImmediate(() => console.log('immediate 2'))
// }

// fn()

// Promise.resolve().then(() => {
//   console.log('promise 0')
//   setTimeout(() => console.log('timeout 0'), 0)
//   setImmediate(() => console.log('immediate 0'))
// })
// Promise.resolve().then(() => {
//   console.log('promise 1')
//   setTimeout(() => console.log('timeout 1'), 0)
//   setImmediate(() => console.log('immediate 1'))
// })
// Promise.resolve().then(() => {
//   console.log('promise 2')
//   setTimeout(() => console.log('timeout 2'), 0)
//   setImmediate(() => console.log('immediate 2'))
// })


setImmediate(function(){
  console.log(1);
},0);
setTimeout(function(){
  console.log(2);
},0);
// new Promise(function(resolve){
//   console.log(3);
//   resolve();
//   console.log(4);
// }).then(function(){
//   console.log(5);
// });
// console.log(6);
// process.nextTick(function(){
//   console.log(7);
// });
// console.log(8);


// process.nextTick(function () {
//   console.log('nextTick 延迟执行1');
// });

// process.nextTick(function () {
//   console.log('nextTick 延迟执行2');
// });

// setImmediate(function () {
//   console.log('setImmediate延迟执行1')
//   process.nextTick(function () {
//       console.log('强势插入');
//   })
// })

// setImmediate(function () {
//   console.log('setImmediate延迟执行2')
// })

// setTimeout(function () {
//   console.log('timeout...');
// },0)

// console.log('正常执行')


// console.log('script start');

// setTimeout(function() {
//     console.log('setTimeout');
// }, 0);

// Promise.resolve().then(function(resolve) {
//     console.log('promise1');
//     setTimeout(function() {
//         console.log('setTimeout in microtask');
//     }, 0);
// }).then(function() {
//     console.log('promise2');
// });

// console.log('script end');
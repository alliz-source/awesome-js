// const start = Date.now()


// setTimeout(function(){
//   console.log('timeout ', Date.now() - start)
//   console.log(2);
// },0);
// setImmediate(function(){
//   console.log('immediate ', Date.now() - start)
//   console.log(1);
// });
// new Promise(function(resolve){
//   console.log(3);
//   resolve();
//   console.log(4);
// }).then(function(){
//   console.log(5);
// });
// console.log(6);
// process.nextTick(function(){
//   console.log(7);
// });
// console.log(8);

// setTimeout(function() {
//   setTimeout(function() {
//     console.log('setTimeout')
//   }, 0);
//   setImmediate(function() {
//     console.log('setImmediate')
//   });
// }, 10);


// function cb(arg) {
//   return function() {
//     console.log(arg);
//     process.nextTick(function() {
//       console.log('nextTick - ' + arg);
//     });
//   }
// }

// cb('0')();
// setImmediate(cb('1'));
// setImmediate(cb('2'));



// function cb(arg) {
//   return function() {
//     console.log(arg);
//     process.nextTick(function() {
//       console.log('nextTick - ' + arg);
//     });
//   }
// }

// setTimeout(cb('0'), 100);
// setTimeout(cb('1'), 100);


// process.nextTick(function() {
//   setTimeout(function() {
//     console.log('timeout')
//   }, 0)
//   setImmediate(function() {
//     console.log('immediate')
//   })
// });

// setTimeout(function() {
//   setTimeout(function() {
//     console.log('setTimeout')
//   }, 0);
//   setImmediate(function() {
//     console.log('setImmediate')
//   });
// }, 10);



// setInterval(function() {
//   setTimeout(function() {
//       console.log('setTimeout3');
//   }, 0);

//   setImmediate(function() {
//       console.log('setImmediate4');
//   });

//   console.log('console1');

//   process.nextTick(function() {
//       console.log('nextTick2');
//   });
// }, 1000)

// setTimeout(() => console.log("timeout 1"), 0);
// setImmediate(() => console.log("immediate 1"));

// let racer1 = function() {
//   setTimeout(() => console.log("timeout"), 0);
//   setImmediate(() => console.log("immediate"));
//   process.nextTick(() => console.log("nextTick"));
// }

// let racer2 = function() {
//   process.nextTick(() => console.log("nextTick"));
//   setTimeout(() => console.log("timeout"), 0);
//   setImmediate(() => console.log("immediate"));
// }

// let racer3 = function() {
//   setImmediate(() => console.log("immediate"));
//   process.nextTick(() => console.log("nextTick"));
//   setTimeout(() => console.log("timeout"), 0);
// }

// racer1()
// racer2()
// racer3()

// (function() {
//   let racer1 = function() {
//     console.log('racer 1')
//     process.nextTick(() => {
//       console.log('tick 1')
//     })
//     Promise.resolve().then(() => console.log('promise 1'))
//     setImmediate(() => console.log("immediate"));
//     setTimeout(() => {
//       console.log("timeout 1")
//       Promise.resolve().then(() => console.log('promise in setTimeout 1'))
//     }, 0);
//   }

// let racer2 = function() {
//   console.log('racer 2')
//   process.nextTick(() => {
//     console.log('tick 2')
//   })
//   Promise.resolve().then(() => console.log('promise 2'))
//   setTimeout(() => {
//     console.log("timeout 2")
//     Promise.resolve().then(() => console.log('promise in setTimeout 2'))
//   }, 1.5);
// }

// let racer3 = function() {
//   console.log('racer 3')
//   process.nextTick(() => {
//     console.log('tick 3')
//   })
//   Promise.resolve().then(() => console.log('promise 3'))
//   setTimeout(() => {
//     console.log("timeout 3")
//     Promise.resolve().then(() => console.log('promise in setTimeout 3'))
//   }, 0);
// }

// racer1()
// racer2()
// racer3()
// })()



// var i = 0;
// function foo(){
//   i++;
//   if(i>20){
//     return;
//   }
//   console.log("foo");
//   setTimeout(()=>{
//     console.log("setTimeout");
//   },0);
//   process.nextTick(foo);
// }
// setTimeout(foo, 2);



var i = 0;
function foo(){
  i++;
  if(i>20){
    return;
  }
  console.log("foo", i);
  setTimeout(()=>{
    console.log("setTimeout", i);
  },0);
  process.nextTick(foo);
}

setTimeout(foo, 2);
setTimeout(()=>{
  console.log("Other setTimeout");
},2);
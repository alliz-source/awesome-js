# ric

React对requestIdleCallback进行了重新实现，原因可以参考[@sebmarkbage tweet](https://twitter.com/sebmarkbage/status/822881464794497024?lang=en)

> Many polyfills get requestIdleCallback wrong. Don't disable when there's interaction/animation. Call it in the idle time in EVERY frame.

同时解释到之所以在ric中使用`postMessage`的原因是，microTasks会阻塞渲染 [tweet](https://twitter.com/longTimeNoTT/status/1103127230585069568)

> It's different semantics. Promise.resolve fires in a micro-task and blocks paint. postMessage lets the browser paint before which is exactly what we want to enable.

## 为什么ric polyfill用50ms来控制

- [Cooperative Scheduling of Background Tasks](https://www.w3.org/TR/requestidlecallback/)
- [Checking for requestIdleCallback](https://developers.google.com/web/updates/2015/08/using-requestidlecallbacks)超简易的polyfill

```js
window.requestIdleCallback =
  window.requestIdleCallback ||
  function (cb) {
    var start = Date.now();
    return setTimeout(function () {
      cb({
        didTimeout: false,
        timeRemaining: function () {
          return Math.max(0, 50 - (Date.now() - start));
        }
      });
    }, 1);
  }

window.cancelIdleCallback =
  window.cancelIdleCallback ||
  function (id) {
    clearTimeout(id);
  }
```

## requestIdleCallback 缺陷
[requestIdleCallback 的缺陷](https://github.com/MuYunyun/blog/blob/master/React/%E4%BD%A0%E4%B8%8D%E7%9F%A5%E9%81%93%E7%9A%84requestIdleCallback.md#requestidlecallback-%E7%9A%84%E7%BC%BA%E9%99%B7)

> requestIdleCallback is called only 20 times per second - Chrome on my 6x2 core Linux machine, it's not really useful for UI work。[Releasing Suspense](https://github.com/facebook/react/issues/13206#issuecomment-418923831)

```js
const start = Date.now()
const fn = deadline => {
  console.log('trigger ', Date.now() - start, deadline.timeRemaining())
  if (Date.now() - start < 1000) {
    requestIdleCallback(fn)
  }
}
requestIdleCallback(fn)

// trigger  1 49.925000000000004
// testIdle:4 trigger  51 49.965
// testIdle:4 trigger  105 49.955000000000005
// testIdle:4 trigger  157 49.945
// testIdle:4 trigger  208 49.945
// testIdle:4 trigger  260 49.92000000000001
// testIdle:4 trigger  313 49.92000000000001
// testIdle:4 trigger  366 49.90500000000001
// testIdle:4 trigger  418 49.945
// testIdle:4 trigger  470 49.93500000000001
// testIdle:4 trigger  522 49.95
// testIdle:4 trigger  575 49.95
// testIdle:4 trigger  628 49.95
// testIdle:4 trigger  683 49.93
// testIdle:4 trigger  737 49.945
// testIdle:4 trigger  789 49.96000000000001
// testIdle:4 trigger  840 49.955000000000005
// testIdle:4 trigger  892 49.95
// testIdle:4 trigger  945 49.95
// testIdle:4 trigger  998 49.940000000000005
// testIdle:4 trigger  1049 49.940000000000005
```

## polyfill

- [Helper classes and methods for implementing the idle-until-urgent pattern https://philipwalton.com/articles/idl…](https://github.com/GoogleChromeLabs/idlize/blob/master/idle-callback-polyfills.mjs)
- [Polyfill for request idle callback. Determines is user interacting with document and fires when it is not or after interaction is finished.](https://github.com/PixelsCommander/requestIdleCallback-polyfill#readme)
- [dan tweet](https://twitter.com/dan_abramov/status/811201516757454848?lang=en)
- [你不知道的requestIdleCallback.md](vhttps://github.com/MuYunyun/blog/blob/master/React/%E4%BD%A0%E4%B8%8D%E7%9F%A5%E9%81%93%E7%9A%84requestIdleCallback.md#requestidlecallback-%E7%9A%84%E7%BC%BA%E9%99%B7)

## 使用requestIdleCallback注意点

1. 对于`deadline.timeRemaining()`是否有一个最大值；上面的例子中已经说了`ric`最大是50ms，以为再大的话，视觉上就会有卡顿现象
2. 在`ric`中最好是处理一些不涉及到DOM变化的操作，因为只有在每一个frame结束以后才会进行`ric`的运行；假如说，frame结束以后再进行DOM更改的话，会造成UI的repaint，在运行时间还有性能上都是很大的损耗。所以在ric中都是进行一些准备工作，或者计算；如果真是需要更改DOM的话，需要将它放置到`requestAnimationFrame`中
3. 在ric中不要使用`Promise.resolve().then`操作，因为他会在ric结束以后马上就执行，这个会在一定程度上延长frame的时间；导致接下来的渲染问题。
4. 推荐在里面放置的是可以预测时间的代码模块

### references

- [你应该知道的requestIdleCallback](https://juejin.im/post/5ad71f39f265da239f07e862)
- [using-requestidlecallback](https://developers.google.com/web/updates/2015/08/using-requestidlecallback)
- [Initial implementation of requestIdleCallback on Android #8569](https://github.com/facebook/react-native/pull/8569)
- [Cooperative Scheduling of Background Tasks API](https://developer.mozilla.org/en-US/docs/Web/API/Background_Tasks_API)又称作requestIdleCallback() API

## 如何使用RIC

```js
function runTaskQueue(deadline) {
  while ((deadline.timeRemaining() > 0 || deadline.didTimeout) && taskList.length) {
    let task = taskList.shift();
    currentTaskNumber++;

    task.handler(task.data);
    scheduleStatusRefresh();
  }

  if (taskList.length) {
    taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000} );
  } else {
    taskHandle = 0;
  }
}
```
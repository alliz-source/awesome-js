# EventLoop

Jake大神详细的介绍[Tasks, microtasks, queues and schedules](https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/)；然后

目前被大家所熟知的是`macroTasks`和`microTasks`，但是实际上，官方并没有`macroTasks`这么一个叫法，而是被称为`task`它会被放到`taskQueue`里面

[理解 JavaScript 中的 macrotask 和 microtask](https://juejin.im/entry/58d4df3b5c497d0057eb99ff)比较详细的描述执行顺序

## 基本概念

[Difference between microtask and macrotask within an event loop context](https://stackoverflow.com/questions/25915634/difference-between-microtask-and-macrotask-within-an-event-loop-context#)给出了下面的基本分类
- `macrotasks`: setTimeout, setInterval, setImmediate, requestAnimationFrame, I/O, UI rendering
- `microtasks`: process.nextTick, Promises, Object.observe, MutationObserver

在一些概念上的划分上，同时可以参考一下[Zone.js's support for standard apis](https://github.com/angular/zone.js/blob/e9f68bedcba044cb0be1a4fbf41fb35b62ca9f25/STANDARD-APIS.md)；它是一个借鉴`Dart`的项目

## setTimeout(fn, 0) vs setImmediate

在浏览器中默认是没有`setImmediate`方法的；可以通过`chrome`中调用验证；但是呢，官方还是提供了一个[setImmediate.js polyfill](https://github.com/YuzuJS/setImmediate)的入口；

## postMessage vs setTimeout

- [postMessage is earlier than setTimeout](https://dbaron.org/log/20100309-faster-timeouts)
`postMessage`和`setTimeout`都是一个macroTask

```js
const startTime = Date.now()
window.addEventListener('message', () => {
  console.log('message ', Date.now() - startTime)
})

setTimeout(() => {
  console.log('timeout', Date.now() - startTime)
}, 0)

window.postMessage('hello')

// message  0
// timeout 1
```

## Promise

## Conclusion

1. microTask比如Promise.then是会阻塞`paint`；这也是为什么[fiber scheduling - ric](https://github.com/facebook/react/pull/8833)中使用postMessage；具体可以参考[tweet](https://twitter.com/sebmarkbage/status/1103132620685160449)
2. 下面是一个比较笼统的时间划分，来源[fiber scheduling - ric](https://github.com/facebook/react/pull/8833)
| frame start time                                      deadline |
[requestAnimationFrame] [layout] [paint] [composite] [postMessage]

## 对UI渲染的影响
首先需要有下面的概念
  1. 每一个task执行完，最终都会进行UI的处理；因为微任务是在UI处理之前，所以最好将对UI造成改变的部分放置到微任务中，这样当task结束，其实你的UI已经更新了；否则会造成再次渲染，可以参考[Vue 中如何使用 MutationObserver 做批量处理？ - 顾轶灵的回答 - 知乎](https://www.zhihu.com/question/55364497/answer/144215284)

1. requestIdleCallback中尽量不要放置导致重绘的操作
2. 影响UI渲染的应该都放置到microTasks中；否则会导致repaint

## Reference

[菲利普·罗伯茨：到底什么是Event Loop呢？ | 欧洲 JSConf 2014](https://www.youtube.com/watch?v=8aGhZQkoFbQ)

[HTML Standard - 8.1.4](https://html.spec.whatwg.org/multipage/webappapis.html#event-loopss)

[Understanding Asynchronous JavaScript — the Event Loop](https://blog.bitsrc.io/understanding-asynchronous-javascript-the-event-loop-74cd408419ff)

## use case

microtask queue: 这个是在[decode() method](https://html.spec.whatwg.org/multipage/embedded-content.html#dom-img-decode)见到的

## video

[what is event loop](https://www.youtube.com/watch?v=8aGhZQkoFbQ)

## MVVM理念

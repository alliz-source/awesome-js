# setImmediate vs setTimeout(fn, 0)

## Introduction

讨论这个问题一般都是针对Node而言，因为[setImmediate](https://developer.mozilla.org/en-US/docs/Web/API/Window/setImmediate)只有在IE中有实现，所以针对browser而言，使用到`setImmediate`的场景更多的是通过是通过polyfill

在[q.js](https://github.com/kriskowal/q/blob/master/q.js)和`vue`都有提到说使用`setImmediate`，但是在web中只有IE支持`setImmediate`；而之所以经常谈及`setImmediate`因为`setTimeout`即使写成`setTimeout(fn, 0)`它其实是触发`timer`依旧会有大约4ms的的延迟，而`setImmediate`的话，能够及时的进行执行；有的时候也会通过`postMessage`来替换掉`setTimeout`

接下来的问题是`setImmediate`和`seTimeout`到底哪个先执行？

- [setTimeout vs setImmediate vs process.nextTick](https://dev.to/logicmason/settimeout-vs-setimmediate-vs-process-nexttick-3lj2)
- [NodeJS - setTimeout(fn,0) vs setImmediate(fn)](https://stackoverflow.com/questions/24117267/nodejs-settimeoutfn-0-vs-setimmediatefn/24119936#24119936)
  - [setImmediate() vs nextTick() vs setTimeout(fn,0) – in depth explanation](http://voidcanvas.com/setimmediate-vs-nexttick-vs-settimeout/)
  - [Node.js event loop workflow & lifecycle in low level](http://voidcanvas.com/nodejs-event-loop/)
- [setImmediate vs process.nextTick](https://cnodejs.org/topic/5556efce7cabb7b45ee6bcac)


## Event loop phase

下面的图片是来自[shigeki - move setImmediate from timer to uv_check #3872](https://github.com/nodejs/node-v0.x-archive/pull/3872#issuecomment-7804775)

![setImmediate](./assets/setImmediate.png)

- [setImmediate() vs nextTick() vs setTimeout(fn,0) – in depth explanation](http://voidcanvas.com/setimmediate-vs-nexttick-vs-settimeout/)和[Phases Overview](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/#phases-overview)对每一个时期的作用都有一个清晰的描述
  - timers: this phase executes callbacks scheduled by setTimeout() and setInterval().
  - pending callbacks: executes I/O callbacks deferred to the next loop iteration.
  - idle, prepare: only used internally.
  - poll: retrieve new I/O events; execute I/O related callbacks (almost all with the exception of close callbacks, the ones scheduled by timers, and setImmediate()); node will block here when appropriate.
  - check: setImmediate() callbacks are invoked here.
  - close callbacks: some close callbacks, e.g. socket.on('close', ...).

## setTimeout的4ms

你经常会发现有些地方说`setTimeout(fn, 0)`其实和`setTimeout(fn, 1)`是一样的，`setTimeout`正常
在下面的几个地方都有提及`setTimeout`即使为0，它其实也有4s的时间；然后经过验证目前在Node.js和chrome中大致更改为了2ms左右

- [mdn Window.setImmediate()](https://developer.mozilla.org/en-US/docs/Web/API/Window/setImmediate)
- [fiber](https://github.com/facebook/react/pull/8833#issuecomment-273974686)
- [Speed up your Websites with a Faster setTimeout using soon()](http://www.bluejava.com/4NS/Speed-up-your-Websites-with-a-Faster-setTimeout-using-soon)
- [setTimeout with a shorter delay](https://dbaron.org/log/20100309-faster-timeouts)

```js
// Node.js
const start = Date.now()
setTimeout(() => console.log('diff ', Date.now() - start), 0)

➜  event-loop git:(master) ✗ node test.js
diff  2
```
In chrome
```js
(function() {
  const start = Date.now()
  setTimeout(() => console.log('diff ', Date.now() - start), 0)
})()

// diff结果为1ms或2ms
```

可以通过查看源码[](https://github.com/nodejs/node-v0.x-archive/blob/master/lib/timers.js#L209)默认会加一个1

```js
exports.setTimeout = function(callback, after) {
  var timer;

  after *= 1; // coalesce to number or NaN

  if (!(after >= 1 && after <= TIMEOUT_MAX)) {
    after = 1; // schedule on next tick, follows browser behaviour
  }

  timer = new Timeout(after);

  if (arguments.length <= 2) {
    timer._onTimeout = callback;
  } else {
    /*
     * Sometimes setTimeout is called with arguments, EG
     *
     *   setTimeout(callback, 2000, "hello", "world")
     *
     * If that's the case we need to call the callback with
     * those args. The overhead of an extra closure is not
     * desired in the normal case.
     */
    var args = Array.prototype.slice.call(arguments, 2);
    timer._onTimeout = function() {
      callback.apply(timer, args);
    }
  }

  if (process.domain) timer.domain = process.domain;

  exports.active(timer);

  return timer;
};
```

## 为什么说异步最好用`setImmediate`而不是`setTimeout`

下面是来自[Node.js event loop workflow & lifecycle in low level](http://voidcanvas.com/nodejs-event-loop/)的例子

```js
var i = 0;
var start = new Date();
function foo () {
  i++;
  if (i < 1000) {
    setImmediate(foo);
  } else {
    var end = new Date();
    console.log("Execution time: ", (end - start));
  }
}
foo();

// 运行时间 23ms
```

用`setImmediate`的话，它的运行时间是`23ms`，然后替换为`setTimeout`进行执行的话运行时间成为了1300ms。按照event loop每一个时期的处理方式而言，应该不会差太多的。在上面的文章中也有描述说，进行`comparison`和`deviation`是一个`CPU`密集型，所以会消耗大量的时间。在每一个timer phase都会进行判断，时间是否到了，然后再进行callback的执行；但是`setImmediate`是没有这种检查的，就好比它就在queue中，直接执行就好。

```js
var i = 0;
var start = new Date();
function foo () {
  i++;
  if (i < 1000) {
    setTimeout(foo, 0);
  } else {
    var end = new Date();
    console.log("Execution time: ", (end - start));
  }
}
foo();
```

## process.nextTick什么时候执行

- [Faster process.nextTick](https://nodejs.org/en/blog/release/v0.10.0/#faster-process-nexttick)

> So, that's what we did. In v0.10, nextTick handlers are run right after each call from C++ into JavaScript. That means that, if your JavaScript code calls process.nextTick, then the callback will fire as soon as the code runs to completion, but before going back to the event loop. The race is over, and all is good.

- [process.nextTick()](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/#process-nexttick) 因为严格来说，`process.nextTick`并不是event loop上的一环，它会在operation结束以后马上被调用

> You may have noticed that process.nextTick() was not displayed in the diagram, even though it's a part of the asynchronous API. This is because process.nextTick() is not technically part of the event loop. Instead, the nextTickQueue will be processed after the current operation is completed, regardless of the current phase of the event loop. Here, an operation is defined as a transition from the underlying C/C++ handler, and handling the JavaScript that needs to be executed.

## 执行顺序

### 连续的setTimeout和setImmediate

```js
setTimeout(() => console.log("setTimeout"), 0);
setTimeout(() => console.log("setTimeout"), 0);
setTimeout(() => console.log("setTimeout"), 0);
setImmediate(() => console.log("setImmediate"));
setImmediate(() => console.log("setImmediate"));
setImmediate(() => console.log("setImmediate"));

// setTimeout
// setTimeout
// setTimeout
// setImmediate
// setImmediate
// setImmediate

// setTimeout
// setImmediate
// setImmediate
// setImmediate
// setTimeout
// setTimeout

// setImmediate
// setImmediate
// setImmediate
// setTimeout
// setTimeout
// setTimeout
```

我们直接执行上面的语句的话，它的最终结果是没有办法确定的，因为你不知道程序执行的时候它到底是处于event loop的哪一个阶段；所以就会造成这种执行顺序的不确定性。按照[The Node.js Event Loop, Timers, and process.nextTick()](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/#setimmediate-vs-settimeout)

> The order in which the timers are executed will vary depending on the context in which they are called. If both are called from within the main module, then timing will be bound by the performance of the process (which can be impacted by other applications running on the machine).

涉及到讨论为什么`setImmediate`和`setTimeout`是不稳定的issue
  1. [timers: set{Immediate,Timeout} and general event loop clarification #5943](https://github.com/nodejs/node-v0.x-archive/issues/5943)
  2. [setTimeout(fn, 0) running before setImmediate #6034](https://github.com/nodejs/node-v0.x-archive/issues/6034)
  3. [[timers] setImmediate executes after setTimeout #25788](https://github.com/nodejs/node-v0.x-archive/issues/25788)
  4. [doc: WIP update timers/nextTick documentation #5950](https://github.com/nodejs/node-v0.x-archive/pull/5950)
  5. [NodeJS - setTimeout(fn,0) vs setImmediate(fn)](https://stackoverflow.com/questions/24117267/nodejs-settimeoutfn-0-vs-setimmediatefn/24119936#24119936)

### Tasks include task/microTask

#### setTimeout中嵌套setImmediate

```js
setTimeout(() => {
  console.log('set timeout 1')
  setImmediate(() => console.log("immediate 1"));
}, 0)
setTimeout(() => {
  console.log('set timeout 2')
  setImmediate(() => console.log("immediate 2"));
}, 0)
setTimeout(() => {
  console.log('set timeout 3')
  setImmediate(() => console.log("immediate 3"));
}, 0)

// set timeout 1
// immediate 1
// set timeout 2
// set timeout 3
// immediate 2
// immediate 3

// set timeout 1
// set timeout 2
// set timeout 3
// immediate 1
// immediate 2
// immediate 3
```

#### setTimeout嵌套setTimeout和setImmediate

- [setTimeout(fn, 0) running before setImmediate #6034](https://github.com/nodejs/node-v0.x-archive/issues/6034#issuecomment-22448855)

```js
setTimeout(function() {
  setTimeout(() => console.log('setTimeout'), 0);
  setTimeout(() => console.log('setTimeout'), 0);
  setTimeout(() => console.log('setTimeout'), 0);
  setImmediate(() => console.log('setImmediate'));
  setImmediate(() => console.log('setImmediate'));
  setImmediate(() => console.log('setImmediate'));
}, 0);

➜  event-loop git:(master) ✗ node test.js
setImmediate
setImmediate
setImmediate
setTimeout
setTimeout
setTimeout
```

上面的结果一直是稳定的；但是如果说，有并列的`setTimeout`的话，顶层两个中的setTimeout或`setImmediate`到底哪个先执行，不能够确定；但是可以确定的是一个context中，`setImmediate`要比`setTimeout`早

```js
setTimeout(function() {
  setTimeout(() => console.log('setTimeout'), 0);
  setTimeout(() => console.log('setTimeout'), 0);
  setTimeout(() => console.log('setTimeout'), 0);
  setImmediate(() => console.log('setImmediate'));
  setImmediate(() => console.log('setImmediate'));
  setImmediate(() => console.log('setImmediate'));
}, 0);

setTimeout(function() {
  setTimeout(() => console.log('setTimeout 1'), 0);
  setTimeout(() => console.log('setTimeout 1'), 0);
  setTimeout(() => console.log('setTimeout 1'), 0);
  setImmediate(() => console.log('setImmediate 1'));
  setImmediate(() => console.log('setImmediate 1'));
  setImmediate(() => console.log('setImmediate 1'));
}, 0);

➜  event-loop git:(master) ✗ node test.js
setImmediate
setImmediate
setImmediate
setImmediate 1
setImmediate 1
setImmediate 1
setTimeout
setTimeout
setTimeout
setTimeout 1
setTimeout 1
setTimeout 1
➜  event-loop git:(master) ✗ node test.js
setImmediate
setImmediate
setImmediate
setTimeout
setTimeout
setTimeout
setImmediate 1
setImmediate 1
setImmediate 1
setTimeout 1
setTimeout 1
setTimeout 1
```

#### setTimeout中嵌套Promise

```js
setTimeout(() => {
  console.log('set timeout 1')
  setImmediate(() => console.log("immediate 1"));
  Promise.resolve().then(() => console.log('promise 1'))
}, 0)
setTimeout(() => {
  console.log('set timeout 2')
  setImmediate(() => console.log("immediate 2"));
  Promise.resolve().then(() => console.log('promise 2'))
}, 0)
setTimeout(() => {
  console.log('set timeout 3')
  setImmediate(() => console.log("immediate 3"));
  Promise.resolve().then(() => console.log('promise 3'))
}, 0)

// set timeout 1
// promise 1
// set timeout 2
// promise 2
// set timeout 3
// promise 3
// immediate 1
// immediate 2
// immediate 3

// set timeout 1
// promise 1
// immediate 1
// set timeout 2
// promise 2
// set timeout 3
// promise 3
// immediate 2
// immediate 3
```

#### macroTask includes microTask

```js
setTimeout(() => {
  console.log('set timeout 1')
  setImmediate(() => console.log("immediate 1"));
  process.nextTick(() => console.log('tick 1'))
}, 0)
setTimeout(() => {
  console.log('set timeout 2')
  setImmediate(() => console.log("immediate 2"));
  process.nextTick(() => console.log('tick 2'))
}, 0)
setTimeout(() => {
  console.log('set timeout 3')
  setImmediate(() => console.log("immediate 3"));
  process.nextTick(() => console.log('tick 3'))
}, 0)

// set timeout 1
// tick 1
// set timeout 2
// tick 2
// set timeout 3
// tick 3
// immediate 1
// immediate 2
// immediate 3

// set timeout 1
// tick 1
// immediate 1
// set timeout 2
// tick 2
// set timeout 3
// tick 3
// immediate 2
// immediate 3
```

### task includes tick recursively

- [Snippet 3 – understanding nextTick() & timer execution](http://voidcanvas.com/nodejs-event-loop/)

```js
var i = 0;
function foo(){
  i++;
  if(i>3){
    return;
  }
  console.log("foo", i);
  setImmediate(()=> console.log("setImmediate", i),0)
  setTimeout(()=> console.log("setTimeout", i), 0);
  process.nextTick(foo);
}

setTimeout(foo, 2);
setTimeout(()=>{
  console.log("Other setTimeout");
},3);

// foo 1
// foo 2
// foo 3
// Other setTimeout
// setTimeout 4
// setImmediate 4
// setImmediate 4
// setImmediate 4
// setTimeout 4
// setTimeout 4

// foo 1
// foo 2
// foo 3
// setImmediate 4
// setImmediate 4
// setImmediate 4
// Other setTimeout
// setTimeout 4
// setTimeout 4
// setTimeout 4

// foo 1
// foo 2
// foo 3
// Other setTimeout
// setImmediate 4
// setImmediate 4
// setImmediate 4
// setTimeout 4
// setTimeout 4
// setTimeout 4
```

### task includes Promise recursively

```js
var i = 0;
function foo(){
  i++;
  if(i>3){
    return;
  }
  console.log("foo", i);
  setImmediate(()=> console.log("setImmediate", i),0)
  setTimeout(()=> console.log("setTimeout", i), 0);
  Promise.resolve().then(foo)
}

setTimeout(foo, 2);
setTimeout(()=>{
  console.log("Other setTimeout");
},3);

// ➜  event-loop git:(master) ✗ node test.js
// foo 1
// foo 2
// foo 3
// Other setTimeout
// setTimeout 4
// setImmediate 4
// setImmediate 4
// setImmediate 4
// setTimeout 4
// setTimeout 4
// ➜  event-loop git:(master) ✗ node test.js
// foo 1
// foo 2
// foo 3
// setImmediate 4
// setImmediate 4
// setImmediate 4
// Other setTimeout
// setTimeout 4
// setTimeout 4
// setTimeout 4
// ➜  event-loop git:(master) ✗ node test.js
// foo 1
// foo 2
// foo 3
// Other setTimeout
// setImmediate 4
// setImmediate 4
// setImmediate 4
// setTimeout 4
// setTimeout 4
// setTimeout 4
```

#### 结论

1. 多个task一起执行时，并不能够确保setTimeout和`setImmediate`到底哪个先执行
2. 如果说task中包含`microTask`的话，那么当执行task的时候，它其中包含的microTask要比同级别的setTimeout优先执行
3. 如果task包含microTask(nextTick, Promise)时，会将micro queue执行完，再执行macroTask

### microTasks may force a next Event loop

```js
let racer1 = function() {
  setTimeout(() => console.log("timeout 1"), 0);
  setImmediate(() => console.log("immediate 1"));
}

let racer2 = function() {
  setTimeout(() => console.log("timeout 2"), 0);
  setImmediate(() => console.log("immediate 2"));
}

let racer3 = function() {
  setImmediate(() => console.log("immediate 3"));
  setTimeout(() => console.log("timeout 3"), 0);
}

racer1()
racer2()
racer3()

// 会出现下面两个结果

// timeout 1 1
// immediate 1 9
// immediate 2 9
// immediate 3 9
// timeout 2 9
// timeout 3 10

// 或者
// immediate 1 1
// immediate 2 6
// immediate 3 6
// timeout 1 6
// timeout 2 6
// timeout 3 7
```

下面的形式和上面是结果是一样的，会有两个结果

```js
let racer1 = function() {
  const start = Date.now()
  setTimeout(() => console.log("timeout 1", Date.now() - start), 0);
  setImmediate(() => console.log("immediate 1", Date.now() - start));
  setTimeout(() => console.log("timeout 2", Date.now() - start), 0);
  setImmediate(() => console.log("immediate 2", Date.now() - start));
  setImmediate(() => console.log("immediate 3", Date.now() - start));
  setTimeout(() => console.log("timeout 3", Date.now() - start), 0);
}

racer1()
```

但是，如果说中间包含一次`process.nextTick`或者`Promise`的话，它们的执行结果就是可预测的了；setTimeout一直发生在`setImmediate`之前

```js
let racer1 = function() {
  const start = Date.now()
  // Promise.resolve().then(() => console.log("promise ", Date.now() - start))
  process.nextTick(() => console.log("next tick", Date.now() - start))
  setTimeout(() => console.log("timeout 1", Date.now() - start), 0);
  setImmediate(() => console.log("immediate 1", Date.now() - start));
  setTimeout(() => console.log("timeout 2", Date.now() - start), 0);
  setImmediate(() => console.log("immediate 2", Date.now() - start));
  setImmediate(() => console.log("immediate 3", Date.now() - start));
  setTimeout(() => console.log("timeout 3", Date.now() - start), 0);
}

racer1()

// next tick 1
// timeout 1 8
// timeout 2 8
// timeout 3 8
// immediate 1 8
// immediate 2 8
// immediate 3 8
```

---更特么诡异的是，下面的形式，又特么失控了

```js
function cb(arg) {
  return function() {
    console.log(arg);
    process.nextTick(function() {
      console.log('nextTick - ' + arg);
    });
  }
}

cb('0')();

process.nextTick(function() {
  console.log('nextTick outer');
});

function fn() {
  process.nextTick(function() {
    console.log('nextTick inner');
  });
  setTimeout(() => console.log('timeout 1'), 0)
  setTimeout(() => console.log('timeout 2'), 0)

  setImmediate(() => console.log('immediate 1'))
  setImmediate(() => console.log('immediate 2'))
}

fn()

// 0
// nextTick - 0
// nextTick outer
// nextTick inner
// timeout 1
// timeout 2
// immediate 1
// immediate 2
// 和下面这种结果
// 0
// nextTick - 0
// nextTick outer
// nextTick inner
// timeout 1
// immediate 1
// immediate 2
// timeout 2
```

### 如何确保稳定的执行顺序

1. 对于`setTimeout`和`setImmediate`到底如何进行确保执行顺序；根据官方的说法，只有在[I/O cycle](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/#setimmediate-vs-settimeout)里面的时候才能够确保`setImmediate`要比`setTimeout`执行的更早

```js
// timeout_vs_immediate.js
const fs = require('fs');

fs.readFile(__filename, () => {
  setTimeout(() => {
    console.log('timeout');
  }, 0);
  setImmediate(() => {
    console.log('immediate');
  });
});

$ node timeout_vs_immediate.js
immediate
timeout
```

2. 在一个setTimeout中连续调用setTimeout和setImmediate时，setImmediate会比`setTimeout`先执行；但是如果说，同样有其它的`setTimeout`也有嵌套的`setTimeout`或`setImmediate`的话，这个时候是没有办法确保，不同的setTimeout main context中，到底哪个`setTimeout`或者`setImmediate`先调用
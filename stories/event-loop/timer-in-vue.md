# Timer in vue

## Introduction

[New timerFunc in rc7 is laggy #3771](https://github.com/vuejs/vue/issues/3771)

- bug: [postMessage](https://github.com/vuejs/vue/blob/9fb105767200f57c9bc371b900c54c87945c2cd0/src/core/util/env.js)
- current: [Promise first then MutationObserver](https://github.com/vuejs/vue/blob/43b489b11f81956fc832ddee4121cfd59287cc39/src/core/util/env.js)

## timerFunc in Vue

其实这个问题，最开始的来源是Vue，有人在[New timerFunc in rc7 is laggy #3771](https://github.com/vuejs/vue/issues/3771)指出碰到一个问题，最终的原因就是`rc7`版本的`Vue`使用了`postMessage`来实现异步的机制，但是因为`postMessage`是一个`macroTask`造成了帧率上的问题；所以，Vue中也提到了如何实现一个异步的机制

### version 2 2016/09/27

```js
// https://github.com/vuejs/vue/blob/43b489b11f81956fc832ddee4121cfd59287cc39/src/core/util/env.js#L47

  // the nextTick behavior leverages the microtask queue, which can be accessed
  // via either native Promise.then or MutationObserver.
  // MutationObserver has wider support, however it is seriously bugged in
  // UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
  // completely stops working after triggering a few times... so, if native
  // Promise is available, we will use it:
  if (typeof Promise !== 'undefined' && isNative(Promise)) {
    var p = Promise.resolve()
    timerFunc = () => {
      p.then(nextTickHandler)
      // in problematic UIWebViews, Promise.then doesn't completely break, but
      // it can get stuck in a weird state where callbacks are pushed into the
      // microtask queue but the queue isn't being flushed, until the browser
      // needs to do some other work, e.g. handle a timer. Therefore we can
      // "force" the microtask queue to be flushed by adding an empty timer.
      if (isIOS) setTimeout(noop)
    }
  } else if (typeof MutationObserver !== 'undefined') {
    // use MutationObserver where native Promise is not available,
    // e.g. IE11, iOS7, Android 4.4
    var counter = 1
    var observer = new MutationObserver(nextTickHandler)
    var textNode = document.createTextNode(String(counter))
    observer.observe(textNode, {
      characterData: true
    })
    timerFunc = () => {
      counter = (counter + 1) % 2
      textNode.data = String(counter)
    }
  } else {
    // fallback to setTimeout
    timerFunc = setTimeout
  }
```

对于它的解释，[Vue 中如何使用 MutationObserver 做批量处理？ - 顾轶灵的回答 - 知乎](https://www.zhihu.com/question/55364497/answer/144215284)有回答；如果说`Promise`支持的话，那么就直接在`Promise.resolve().then`中来进行异步机制实现；亦或者用`MutationObserver`来替补
> 其实就是创建一个 TextNode 并监听内容变化，然后要 nextTick 的时候去改一下这个节点的文本内容

最后如何都没有的话就用`setTimeout`来实现；其中有一个插曲就是试图通过`setImmediately`来解决问题，`MessageChannel`解决掉一些问题，这里的解决方式依旧是`Promise`来处理`microTask`但是呢，如果不支持`Promise`的话，就直接fallback到`macroTask`这个就是造成[New timerFunc in rc7 is laggy #3771](https://github.com/vuejs/vue/issues/3771)的原因；所以，新的处理方式中对microTask增加了`MutationObserver`的应用，因为`MutatationObserver`同样是一个`microTask`

### version 3 2017/10/14

```js
// https://github.com/vuejs/vue/blob/v2.5.5/src/core/util/next-tick.js#L20
// Here we have async deferring wrappers using both micro and macro tasks.
// In < 2.4 we used micro tasks everywhere, but there are some scenarios where
// micro tasks have too high a priority and fires in between supposedly
// sequential events (e.g. #4521, #6690) or even between bubbling of the same
// event (#6566). However, using macro tasks everywhere also has subtle problems
// when state is changed right before repaint (e.g. #6813, out-in transitions).
// Here we use micro task by default, but expose a way to force macro task when
// needed (e.g. in event handlers attached by v-on).
let microTimerFunc
let macroTimerFunc
let useMacroTask = false

// Determine (macro) Task defer implementation.
// Technically setImmediate should be the ideal choice, but it's only available
// in IE. The only polyfill that consistently queues the callback after all DOM
// events triggered in the same loop is by using MessageChannel.
/* istanbul ignore if */
if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  macroTimerFunc = () => {
    setImmediate(flushCallbacks)
  }
} else if (typeof MessageChannel !== 'undefined' && (
  isNative(MessageChannel) ||
  // PhantomJS
  MessageChannel.toString() === '[object MessageChannelConstructor]'
)) {
  const channel = new MessageChannel()
  const port = channel.port2
  channel.port1.onmessage = flushCallbacks
  macroTimerFunc = () => {
    port.postMessage(1)
  }
} else {
  /* istanbul ignore next */
  macroTimerFunc = () => {
    setTimeout(flushCallbacks, 0)
  }
}

// Determine MicroTask defer implementation.
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  const p = Promise.resolve()
  microTimerFunc = () => {
    p.then(flushCallbacks)
    // in problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) setTimeout(noop)
  }
} else {
  // fallback to macro
  microTimerFunc = macroTimerFunc
}
```

### version 4 2019/10/08
```js
// https://github.com/vuejs/vue/blob/dev/src/core/util/next-tick.js#L22
// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
let timerFunc

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  const p = Promise.resolve()
  timerFunc = () => {
    p.then(flushCallbacks)
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) setTimeout(noop)
  }
  isUsingMicroTask = true
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  let counter = 1
  const observer = new MutationObserver(flushCallbacks)
  const textNode = document.createTextNode(String(counter))
  observer.observe(textNode, {
    characterData: true
  })
  timerFunc = () => {
    counter = (counter + 1) % 2
    textNode.data = String(counter)
  }
  isUsingMicroTask = true
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = () => {
    setImmediate(flushCallbacks)
  }
} else {
  // Fallback to setTimeout.
  timerFunc = () => {
    setTimeout(flushCallbacks, 0)
  }
}
```

### version 1 2016/9/24

```js
/**
 * Defer a task to execute it asynchronously. Ideally this
 * should be executed as a microtask, but MutationObserver is unreliable
 * in iOS UIWebView so we use a setImmediate shim and fallback to setTimeout.
 */
export const nextTick = (function () {
  let callbacks = []
  let pending = false
  let timerFunc

  function nextTickHandler () {
    pending = false
    const copies = callbacks.slice(0)
    callbacks = []
    for (let i = 0; i < copies.length; i++) {
      copies[i]()
    }
  }

  /* istanbul ignore else */
  if (inBrowser && window.postMessage &&
    !window.importScripts && // not in WebWorker
    !(isAndroid && !window.requestAnimationFrame) // not in Android <= 4.3
  ) {
    const NEXT_TICK_TOKEN = '__vue__nextTick__'
    window.addEventListener('message', e => {
      if (e.source === window && e.data === NEXT_TICK_TOKEN) {
        nextTickHandler()
      }
    })
    timerFunc = () => {
      window.postMessage(NEXT_TICK_TOKEN, '*')
    }
  } else {
    timerFunc = (typeof global !== 'undefined' && global.setImmediate) || setTimeout
  }

  return function queueNextTick (cb: Function, ctx?: Object) {
    const func = ctx
      ? function () { cb.call(ctx) }
      : cb
    callbacks.push(func)
    if (pending) return
    pending = true
    timerFunc(nextTickHandler, 0)
  }
})()
```

### 总结
- [Vue 中如何使用 MutationObserver 做批量处理？](https://www.zhihu.com/question/55364497)
- [Vue源码详解之nextTick：MutationObserver只是浮云，microtask才是核心！ #6](https://github.com/Ma63d/vue-analysis/issues/6)

上面的文章也都有相应的描述，其实Vue在这里需要解决的问题就是`nextTick`而它最好是在`microTask`中进行驱动；所以问题就回归到了哪些方案更适合写异步操作；

1. `MutationObserver`这个在最开始的代码库有描述，因为它在iOS UI存在一些问题所以就直接使用了`postMessage`
2. 没过多久就升级到了另一个版本，首先使用`Promise`；然后再用`MutationObserver`作为一个fallback
3. 版本3，就讨论出了一个问题，假如说对于`microTask`因为它的权重太高，有可能说会影响到`bubble`有的时候甚至是同一个event[#6566](https://github.com/vuejs/vue/issues/6566)；而如果使用macro的话，也会存在问题，比如状态的改变发生在`repaint`之前（这一点不是太理解）；

### MessageChannel使用场景

针对提供异步机制，在其它应用中也有使用；比如[A promise library for JavaScript q.js](https://github.com/kriskowal/q/blob/master/q.js);它对于异步机制的实现是通过`MessageChannel`来实现的

因为Q需要支持在Node.js中的场景，而在Node.js中`process.nextTick`会是一个更好的选择；其次是setImmediate，因为`setImmediate`要比`setTimeout`更好，因为它执行的更早；

```js
    nextTick = function (task) {
        tail = tail.next = {
            task: task,
            domain: isNodeJS && process.domain,
            next: null
        };

        if (!flushing) {
            flushing = true;
            requestTick();
        }
    };

    if (typeof process === "object" &&
        process.toString() === "[object process]" && process.nextTick) {
        // Ensure Q is in a real Node environment, with a `process.nextTick`.
        // To see through fake Node environments:
        // * Mocha test runner - exposes a `process` global without a `nextTick`
        // * Browserify - exposes a `process.nexTick` function that uses
        //   `setTimeout`. In this case `setImmediate` is preferred because
        //    it is faster. Browserify's `process.toString()` yields
        //   "[object Object]", while in a real Node environment
        //   `process.toString()` yields "[object process]".
        isNodeJS = true;

        requestTick = function () {
            process.nextTick(flush);
        };

    } else if (typeof setImmediate === "function") {
        // In IE10, Node.js 0.9+, or https://github.com/NobleJS/setImmediate
        if (typeof window !== "undefined") {
            requestTick = setImmediate.bind(window, flush);
        } else {
            requestTick = function () {
                setImmediate(flush);
            };
        }
    } else if (typeof MessageChannel !== "undefined") {
        // modern browsers
        // http://www.nonblocking.io/2011/06/windownexttick.html
        var channel = new MessageChannel();
        // At least Safari Version 6.0.5 (8536.30.1) intermittently cannot create
        // working message ports the first time a page loads.
        channel.port1.onmessage = function () {
            requestTick = requestPortTick;
            channel.port1.onmessage = flush;
            flush();
        };
        var requestPortTick = function () {
            // Opera requires us to provide a message payload, regardless of
            // whether we use it.
            channel.port2.postMessage(0);
        };
        requestTick = function () {
            setTimeout(flush, 0);
            requestPortTick();
        };

    } else {
        // old browsers
        requestTick = function () {
            setTimeout(flush, 0);
        };
    }
    // runs a task after all other tasks have been run
    // this is useful for unhandled rejection tracking that needs to happen
    // after all `then`d tasks have been run.
    nextTick.runAfter = function (task) {
        laterQueue.push(task);
        if (!flushing) {
            flushing = true;
            requestTick();
        }
    };
    return nextTick;
```
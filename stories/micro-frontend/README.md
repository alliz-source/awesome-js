# micro frontend

- [Micro Frontends](https://martinfowler.com/articles/micro-frontends.html)
- [Adopting a Micro-frontends architecture](https://medium.com/dazn-tech/adopting-a-micro-frontends-architecture-e283e6a3c4f3)
- [I don’t understand micro-frontends.](https://medium.com/@lucamezzalira/i-dont-understand-micro-frontends-88f7304799a9)
- [Microfrontends: the good, the bad, and the ugly](https://codeburst.io/microfrontends-the-good-the-bad-and-the-ugly-14faff93aea5)
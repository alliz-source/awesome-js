## How to export and import

## react-navigation

```js
// https://github.com/react-navigation/stack/blob/master/src/index.tsx
/**
 * Views
 */
export { default as Header } from './views/Header/Header';
export { default as HeaderTitle } from './views/Header/HeaderTitle';
export { default as HeaderBackButton } from './views/Header/HeaderBackButton';


// https://github.com/react-navigation/react-navigation/blob/master/src/react-navigation.js
module.exports = {
  // Header
  get Header() {
    return require('react-navigation-stack').Header;
  },
  get HeaderTitle() {
    return require('react-navigation-stack').HeaderTitle;
  },
  get HeaderBackButton() {
    return require('react-navigation-stack').HeaderBackButton;
  },
}
```

## react

```js
// https://github.com/facebook/react/blob/master/packages/react/index.js

const React = require('./src/React');

// TODO: decide on the top-level export form.
// This is hacky but makes it work with both Rollup and Jest.
module.exports = React.default || React;
```

## dynamic import

- [Dynamic imports, React and Redux](https://codeburst.io/dynamic-imports-react-and-redux-29f6d2d88d77)
- [JavaScript modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)
- [Requiring modules in Node.js: Everything you need to know](https://www.freecodecamp.org/news/requiring-modules-in-node-js-everything-you-need-to-know-e7fbd119be8/)
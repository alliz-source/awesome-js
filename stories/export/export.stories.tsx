import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import README from './README.md'

storiesOf('/Export/docs', module)
  .add('how to export', () => <></>, {
    notes: { markdown: README }
  })
